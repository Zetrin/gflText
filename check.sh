#!/usr/bin/env bash

echo "Checking by PVS Studio"
	pvs-studio-analyzer analyze -f build/compile_commands.json  -o gfl-tools_pvs.log > /dev/null
	plog-converter -a GA:1,2 -t errorfile -o pvs.tasks gfl-tools_pvs.log > /dev/null

#echo "Checking by clang-check"
#	find src/ -type f -name "*.c" | xargs clang-check -p
