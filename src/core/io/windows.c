#if defined(_WIN32) || defined(_WIN64)
#include <stdio.h>
#include <windows.h>

#include "../common.h"
#include "../io.h"

int make_path(const char *const path)
{
	if (CreateDirectory(temp, nullptr) == FALSE) {
		if (GetLastError() != ERROR_ALREADY_EXISTS) {
			return -1;
		}
	}
}

static void close_file(struct io_state *s)
{
}

static void mmap_close_file(struct io_state *s)
{
	UnmapViewOfFile(s->data);
	CloseHandle(s->mapping);
	CloseHandle(s->file);
}

static bool mmap_open_read(const char *path, struct io_state *s)
{
	HANDLE hFile;
	DWORD dwFileSize;
	HANDLE hMapping;
	void *addr;

	hFile = CreateFile(path, GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile == INVALID_HANDLE_VALUE) {
		return false;
	}

	dwFileSize = GetFileSize(hFile, nullptr);
	if (dwFileSize == INVALID_FILE_SIZE) {
		CloseHandle(hFile);
		return false;
	}

	hMapping = CreateFileMapping(hFile, nullptr, PAGE_READONLY, 0, 0, nullptr);
	if (hMapping == nullptr) {
		CloseHandle(hFile);
		return false
	}

	addr = MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, dwFileSize);
	if (addr == nullptr) {
		CloseHandle(hMapping);
		CloseHandle(hFile);
		return false;
	}

	s->data = addr;
	s->size = dwFileSize;
	s->offset = 0;
	s->free = mmap_close_file;
	s->mapping = hMapping;
	s->file = hFile;

	return true;
}

static int write_file(writer *w, void *buffer, size_t size)
{
}

bool file_open_write(const char *path, struct io_state *s)
{
}
#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
