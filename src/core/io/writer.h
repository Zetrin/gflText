#ifndef WRITER_H
#define WRITER_H

#include <setjmp.h>
#include <stddef.h>
#include <stdint.h>

struct writer;

typedef void (*write_fn)(struct writer *w, const void *buffer, const size_t size);
typedef void (*writer_align_fn)(struct writer *w, const size_t align);
typedef void (*writer_seek_fn)(struct writer *w, const uint64_t offset);

typedef void (*write8_fn)(struct writer *w, const uint8_t val);
typedef void (*write16_fn)(struct writer *w, const uint16_t val);
typedef void (*write32_fn)(struct writer *w, const uint32_t val);
typedef void (*write64_fn)(struct writer *w, const uint64_t val);

struct writer {
	write_fn write;
	write16_fn write16;
	write32_fn write32;
	write64_fn write64;
	writer_align_fn align;
	writer_seek_fn seek;
	void *data;
	int fd;
	size_t size;
	size_t offset;
	void (*free)(struct writer *w);
	bool jmp_set;
	sigjmp_buf jmp_buf;
};

struct writer *writer_create_memory_writer(void);
struct writer *writer_create_file_writer(const char *path);

void writer_free(struct writer *w);
void writer_close(struct writer *w);
struct writer writer_clone(struct writer *w);

void writer_set_write_memory(struct writer *w);
bool writer_set_write_file(struct writer *w, const char *path);

uint64_t writer_tell(struct writer *w);
void writer_seek(struct writer *w, const uint64_t seek);
void writer_skip(struct writer *w, const uint32_t length);
void writer_align(struct writer *w, const size_t align);

void writer_put_8(struct writer *w, const uint8_t val);
void writer_put_16(struct writer *w, const uint16_t val);
void writer_put_32(struct writer *w, const uint32_t val);
void writer_put_64(struct writer *w, const uint64_t val);

void writer_put_be16(struct writer *w, const uint16_t val);
void writer_put_be32(struct writer *w, const uint32_t val);
void writer_put_be64(struct writer *w, const uint64_t val);

void writer_put_le16(struct writer *w, const uint16_t val);
void writer_put_le32(struct writer *w, const uint32_t val);
void writer_put_le64(struct writer *w, const uint64_t val);

void writer_put_bytes(struct writer *w, const void *data, const size_t size);
void writer_put_string(struct writer *w, const char *str);

void writer_set_big_endian(struct writer *w);
void writer_set_little_endian(struct writer *w);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
