#include <errno.h>
#include <fcntl.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "writer.h"

#if (__GNUC__ >= 3)
#define unlikely(cond) __builtin_expect((cond), 0)
#define likely(cond)   __builtin_expect((cond), 1)
#else
#define unlikely(cond) (cond)
#define likely(cond)   (cond)
#endif

// #ifdef __LINUX__
#if 1

[[maybe_unused]]
static void close_file(struct writer *w)
{
	if (likely(ftruncate(w->fd, w->size) == 0)) {
		fsync(w->fd);
		close(w->fd);
	} else {
		perror("Error: ftruncate failed");
	}
}

[[maybe_unused]]
static void free_memory(struct writer *w)
{
	free(w->data);
}

[[maybe_unused]]
bool file_open_write(const char *path, struct writer *w)
{
	int fd;
	struct stat st;

	fd = open(path, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
	if (unlikely(fd == -1)) {
		perror("Error: can not open file to write");
		return false;
	}

	if (unlikely(fstat(fd, &st) == -1)) {
		perror("Error: can not stat file");
		return false;
	}

	w->fd = fd;
	w->size = st.st_size;
	w->offset = 0;
	w->data = nullptr;
	w->free = close_file;

	return true;
}

static void write_file(struct writer *w, const void *buffer, const size_t size)
{
	size_t check = pwrite(w->fd, buffer, size, w->offset);
	if (unlikely(check != size)) {
		fprintf(stderr, "Error: can not write bundle data: %s\n", strerror(errno));
		longjmp(w->jmp_buf, 0);
	} else {
		w->offset += size;
		if (w->offset > w->size)
			w->size = w->offset;
	}
}
#endif

static void seek_common(struct writer *w, uint64_t offset)
{
	if (unlikely(w->size < offset))
		offset = w->size - 1;

	w->offset = offset;
}

static void seek_output_mem(struct writer *w, const uint64_t offset)
{
	if (likely(w->offset + offset > w->size)) {
		void *tmp = realloc(w->data, w->offset + offset);
		if (likely(tmp != nullptr)) {
			memset(tmp + w->size, 0, offset - w->size);
			w->size = offset;
			w->offset = offset;
			w->data = tmp;
		} else {
			perror("Error: can not seek to memory");
		}
	} else {
		longjmp(w->jmp_buf, 0);
	}
}

uint64_t writer_tell(struct writer *w)
{
	return w->offset;
}

static inline size_t __align(const uint64_t offset, const unsigned n)
{
	return ((offset + (n - 1)) & ~(n - 1)) - offset;
}

static void align_output_mem(struct writer *w, const size_t align)
{
	const size_t padding = __align(w->offset, align);

	if (unlikely(w->offset + padding > w->size)) {
		longjmp(w->jmp_buf, 0);
	} else {
		memset(w->data + w->offset, 0, padding);
		w->offset += padding;
	}
}

static void align_output_file(struct writer *w, const size_t align)
{
	const size_t padding = __align(w->offset, align);
	w->size += padding;
	// @TODO truncate
}

static void write_memory(struct writer *w, const void *buffer, const size_t size)
{
	void *tmp = realloc(w->data, w->size + size);
	if (likely(tmp != nullptr)) {
		w->data = tmp;
		memcpy(w->data + w->offset, buffer, size);
		w->offset += size;
		w->size = w->offset;
	} else
		longjmp(w->jmp_buf, 0);
}

void writer_put_8(struct writer *w, const uint8_t value)
{
	w->write(w, &value, sizeof(value));
}

void writer_put_16(struct writer *w, const uint16_t val)
{
	w->write16(w, val);
}

void writer_put_32(struct writer *w, const uint32_t val)
{
	w->write32(w, val);
}

void writer_put_64(struct writer *w, const uint64_t val)
{
	w->write64(w, val);
}

void writer_put_le16(struct writer *w, const uint16_t value)
{
	w->write(w, &value, sizeof(value));
}

void writer_put_be16(struct writer *w, const uint16_t value)
{
	const uint16_t val = htobe16(value);
	w->write(w, &val, sizeof(val));
}

void writer_put_le32(struct writer *w, uint32_t value)
{
	w->write(w, &value, sizeof(value));
}

void writer_put_be32(struct writer *w, const uint32_t value)
{
	const uint32_t val = htobe32(value);
	w->write(w, &val, sizeof(val));
}

void writer_put_le64(struct writer *w, const uint64_t value)
{
	w->write(w, &value, sizeof(value));
}

void writer_put_be64(struct writer *w, const uint64_t value)
{
	const uint64_t val = htobe64(value);
	w->write(w, &val, sizeof(val));
}

void writer_put_bytes(struct writer *w, const void *data, const size_t size)
{
	w->write(w, data, size);
}

void writer_put_string(struct writer *w, const char *str)
{
	const uint64_t len = strlen(str);
	const char e = '\0';

	w->write(w, str, len);
	w->write(w, &e, 1);
}

inline void writer_align(struct writer *w, const size_t align)
{
	w->align(w, align);
}

static void writer_init(struct writer *w)
{
	memset(w, 0, sizeof(*w));
#if __BYTE_ORDER == __LITTLE_ENDIAN
	w->write16 = writer_put_le16;
	w->write32 = writer_put_le32;
	w->write64 = writer_put_le64;
#else
	r->read16 = reader_get_be16;
	r->read32 = reader_get_be32;
	r->read64 = reader_get_be64;
#endif
}

void writer_set_big_endian(struct writer *r)
{
	r->write16 = writer_put_be16;
	r->write32 = writer_put_be32;
	r->write64 = writer_put_be64;
}

void writer_set_little_endian(struct writer *r)
{
	r->write16 = writer_put_le16;
	r->write32 = writer_put_le32;
	r->write64 = writer_put_le64;
}

void writer_set_write_memory(struct writer *w)
{
	writer_init(w);

	w->write = write_memory;
	w->align = align_output_mem;
	w->seek = seek_output_mem;
	w->free = free_memory;
}

bool writer_set_write_file(struct writer *w, const char *path)
{
	// w->state.free = close_file;
	writer_init(w);

	w->write = write_file;
	w->align = align_output_file;
	w->seek = seek_common;
	w->free = close_file;

	return file_open_write(path, w);
}

struct writer *writer_create_memory_writer(void)
{
	struct writer *w = malloc(sizeof(*w));
	if (unlikely(w == nullptr)) {
		perror("Error: can not allocate memory for writer");
		return nullptr;
	}

	writer_set_write_memory(w);

	return w;
}

struct writer *writer_create_file_writer(const char *path)
{
	struct writer *w = malloc(sizeof(*w));
	if (unlikely(w == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	if (writer_set_write_file(w, path)) {
		return w;
	} else {
		free(w);
		return nullptr;
	}
}

void writer_close(struct writer *w)
{
	if (w->free != nullptr)
		w->free(w);
}

void writer_free(struct writer *w)
{
	writer_close(w);
	free(w);
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
