#include <endian.h>
#include <fcntl.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "reader.h"

#if (__GNUC__ >= 3)
#define unlikely(cond) __builtin_expect((cond), 0)
#define likely(cond)   __builtin_expect((cond), 1)
#else
#define unlikely(cond) (cond)
#define likely(cond)   (cond)
#endif

// #ifdef __LINUX__
#if 1
static void mmap_close_file(struct reader *r)
{
	munmap(r->data, r->size);
}

static bool mmap_open_read(const char *path, struct reader *r)
{
	int fd;
	struct stat st;
	void *addr;

	fd = open(path, O_RDONLY | O_NOFOLLOW);
	if (unlikely(fd == -1)) {
		perror("Error: can not open file");
		return false;
	}

	if (unlikely(fstat(fd, &st) == -1)) {
		perror("Error: can not stat file");
		close(fd);
		return false;
	}

	addr = mmap(nullptr, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (unlikely(addr == MAP_FAILED)) {
		perror("Error: can not mmap file");
		close(fd);
		return false;
	}

	close(fd);

	r->data = addr;
	r->size = st.st_size;
	r->free = mmap_close_file;

	return true;
}
#endif

sigjmp_buf *reader_on_error(struct reader *r)
{
	return &(r->jmp_buf);
}

static void __read(struct reader *r, void *restrict buffer, const size_t size)
{
	if (unlikely(r->offset + size > r->size)) {
		fprintf(stderr, "Out of bounds\n");
		longjmp(r->jmp_buf, 0);
	} else {
		const void *data_ptr = r->data + r->offset;
		memcpy(buffer, data_ptr, size);
		r->offset += size;
	}
}

void reader_skip(struct reader *r, const uint64_t length)
{
	if (r->offset + length >= r->size)
		longjmp(r->jmp_buf, 0);

	r->offset += length;
}

uint64_t reader_tell(struct reader *r)
{
	return r->offset;
}

void reader_seek(struct reader *r, const uint64_t offset)
{
	if (likely((size_t)offset < r->size))
		r->offset = offset;
	else
		longjmp(r->jmp_buf, 0);
}

uint8_t reader_get_8(struct reader *r)
{
	uint8_t val;
	__read(r, &val, sizeof(val));
	return val;
}

uint16_t reader_get_le16(struct reader *r)
{
	uint16_t val;
	__read(r, &val, sizeof(val));
	return le16toh(val);
}

uint16_t reader_get_be16(struct reader *r)
{
	uint16_t val;
	__read(r, &val, sizeof(val));
	return be16toh(val);
}

uint32_t reader_get_le32(struct reader *r)
{
	uint32_t val;
	__read(r, &val, sizeof(val));
	return le32toh(val);
}

uint32_t reader_get_be32(struct reader *r)
{
	uint32_t val;
	__read(r, &val, sizeof(val));
	return be32toh(val);
}

uint64_t reader_get_le64(struct reader *r)
{
	uint64_t val;
	__read(r, &val, sizeof(val));
	return le64toh(val);
}

uint64_t reader_get_be64(struct reader *r)
{
	uint64_t val;
	__read(r, &val, sizeof(val));
	return be64toh(val);
}

inline uint16_t reader_get_16(struct reader *r)
{
	return r->read16(r);
}

inline uint32_t reader_get_32(struct reader *r)
{
	return r->read32(r);
}

inline uint64_t reader_get_64(struct reader *r)
{
	return r->read64(r);
}

void reader_get_bytes(struct reader *r, void *data, const size_t size)
{
	__read(r, data, size);
}

size_t reader_strlen(struct reader *r)
{
	char c;
	ssize_t len = 0;
	const uint64_t offset = r->offset;

	do {
		c = reader_get_8(r);
		if (c == 0)
			break;
		len++;
	} while (1);

	r->offset = offset;

	return len;
}

inline void reader_get_string(struct reader *r, char *restrict dst, const size_t maxlen)
{
	size_t len = reader_strlen(r) + 1;

	if (len >= maxlen)
		len = maxlen - 1;

	__read(r, dst, len);
}

size_t align(const uint64_t offset, const unsigned n)
{
	return ((offset + (n - 1)) & ~(n - 1)) - offset;
}

void reader_align(struct reader *r, const size_t by)
{
	const size_t padding = align(r->offset, by);

	if (unlikely(r->offset + padding < r->size)) {
		r->offset += padding;
	} else {
		if (r->jmp_set)
			longjmp(r->jmp_buf, 0);
	}
}

static void reader_init(struct reader *r)
{
	memset(r, 0, sizeof(*r));
#if __BYTE_ORDER == __LITTLE_ENDIAN
	r->read16 = reader_get_le16;
	r->read32 = reader_get_le32;
	r->read64 = reader_get_le64;
#else
	r->read16 = reader_get_be16;
	r->read32 = reader_get_be32;
	r->read64 = reader_get_be64;
#endif
}

bool reader_set_read_file(struct reader *r, const char *path)
{
	reader_init(r);

	return mmap_open_read(path, r);
}

bool reader_set_read_memory(struct reader *r, void *data, const size_t size)
{
	if (data == nullptr || size == 0)
		return false;

	reader_init(r);

	r->data = (void *)data;
	r->size = size;
	r->free = nullptr;
	return true;
}

void reader_set_big_endian(struct reader *r)
{
	r->read16 = reader_get_be16;
	r->read32 = reader_get_be32;
	r->read64 = reader_get_be64;
}

void reader_set_little_endian(struct reader *r)
{
	r->read16 = reader_get_le16;
	r->read32 = reader_get_le32;
	r->read64 = reader_get_le64;
}

void reader_close(struct reader *r)
{
	if (r->free != nullptr)
		r->free(r);
}

void reader_free(struct reader *r)
{
	reader_close(r);
	free(r);
}

struct reader *reader_create_memory_reader(void *data, const size_t size)
{
	struct reader *r = malloc(sizeof(*r));
	if (unlikely(r == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	reader_set_read_memory(r, data, size);

	return r;
}

struct reader *reader_create_file_reader(const char *path)
{
	struct reader *r = malloc(sizeof(*r));
	if (unlikely(r == nullptr)) {
		perror("Error: can not allocate memory for reader");
		return nullptr;
	}

	if (likely(reader_set_read_file(r, path)))
		return r;
	else {
		reader_free(r);
		return nullptr;
	}
}

bool reader_clone(struct reader *origin, struct reader *copy, const uint64_t offset, const size_t size)
{

	if (origin->offset + size >= origin->size)
		return false;

	reader_init(copy);

	copy->data = origin->data + offset;
	copy->size = size;
	copy->read16 = origin->read16;
	copy->read32 = origin->read32;
	copy->read64 = origin->read64;

	return true;
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
