#ifndef READER_H
#define READER_H

#include <setjmp.h>
#include <stddef.h>
#include <stdint.h>

struct reader;

typedef void (*reader_read_fn)(struct reader *r, void *buffer, const size_t size);
typedef unsigned int (*reader_align_fn)(struct reader *r, const size_t align);
typedef uint64_t (*reader_seek_fn)(struct reader *r, const uint64_t offset);

typedef uint8_t (*read8_fn)(struct reader *r);
typedef uint16_t (*read16_fn)(struct reader *r);
typedef uint32_t (*read32_fn)(struct reader *r);
typedef uint64_t (*read64_fn)(struct reader *r);

struct reader {
	read16_fn read16;
	read32_fn read32;
	read64_fn read64;
	void *data;
	size_t size;
	size_t offset;
	void (*free)(struct reader *r);
	bool jmp_set;
	sigjmp_buf jmp_buf;
};

struct reader *reader_create_memory_reader(void *data, const size_t size);
struct reader *reader_create_file_reader(const char *path);
bool reader_set_read_memory(struct reader *r, void *data, const size_t size);
bool reader_set_read_file(struct reader *r, const char *path);
bool reader_clone_memory_area(struct reader *r, void const *data, const size_t size);

void reader_close(struct reader *r);
void reader_free(struct reader *r);

bool reader_clone(struct reader *origin, struct reader *copy, const uint64_t offset, const size_t size);

uint64_t reader_tell(struct reader *r);
void reader_seek(struct reader *r, const uint64_t offset);
void reader_skip(struct reader *r, const uint64_t length);
void reader_align(struct reader *r, const size_t align);

uint8_t reader_get_8(struct reader *r);
uint16_t reader_get_16(struct reader *r);
uint32_t reader_get_32(struct reader *r);
uint64_t reader_get_64(struct reader *r);

uint16_t reader_get_be16(struct reader *r);
uint32_t reader_get_be32(struct reader *r);
uint64_t reader_get_be64(struct reader *r);

uint16_t reader_get_le16(struct reader *r);
uint32_t reader_get_le32(struct reader *r);
uint64_t reader_get_le64(struct reader *r);

void reader_get_bytes(struct reader *r, void *data, const size_t size);

size_t reader_strlen(struct reader *r);
void reader_get_string(struct reader *r, char *dst, const size_t maxlen);

void reader_set_big_endian(struct reader *r);
void reader_set_little_endian(struct reader *r);

sigjmp_buf *reader_on_error(struct reader *r);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
