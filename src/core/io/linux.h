#ifndef LINUX_H
#define LINUX_H

int make_path(const char *const path);
void close_file(struct io_state *s);
void mmap_close_file(struct io_state *s);
bool mmap_open_read(const char *path, struct io_state *s);
bool file_open_write(const char *path, struct io_state *s);
// int write_file(writer *w, const void *buffer, size_t size);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
