#include <malloc.h>
#include <string.h>

#include "../io/writer.h"
#include "font.h"

font_asset *parse_font_asset(struct reader *r)
{
	font_asset *font = malloc(sizeof(*font));
	if (font == nullptr) {
		perror("Could not allocate memory for font");
		return nullptr;
	}

	memset(font, 0, sizeof(*font));

	font->title_length = reader_get_32(r);
	reader_get_bytes(r, font->title, font->title_length);
	reader_align(r, 4);
	font->line_spacing = reader_get_32(r);
	font->material.file_id = reader_get_32(r);
	font->material.path_id = reader_get_64(r);
	font->font_size = reader_get_32(r);
	font->texture.file_id = reader_get_32(r);
	font->texture.path_id = reader_get_64(r);
	font->ascii_offset = reader_get_32(r);
	font->tracking = reader_get_32(r);
	font->char_spacing = reader_get_32(r);
	font->char_padding = reader_get_32(r);
	font->convert_case = reader_get_32(r);
	font->char_rect_size = reader_get_32(r);
	if (font->char_rect_size != 0) {
		font->rects = calloc(font->char_rect_size, sizeof(*font->rects));
		if (font->rects == nullptr) {
			free(font);
			return nullptr;
		}

		for (uint32_t i = 0; i < font->char_rect_size; i++) {
			font->rects[i].index = reader_get_32(r);
			font->rects[i].uv.x = reader_get_32(r);
			font->rects[i].uv.y = reader_get_32(r);
			font->rects[i].uv.w = reader_get_32(r);
			font->rects[i].uv.h = reader_get_32(r);
			font->rects[i].advice = reader_get_32(r);
			font->rects[i].flipped = reader_get_8(r);
			reader_align(r, 4);
		}
		// offset += font->char_rect_size * 44;
	}
	font->kerning_size = reader_get_32(r);
	if (font->kerning_size > 0) {
		font->kerning = calloc(font->kerning_size, sizeof(*font->kerning));
		if (font->kerning == nullptr) {
			free(font->rects);
			free(font);
			return nullptr;
		}

		for (uint32_t i = 0; i < font->kerning_size; i++) {
			font->kerning[i].x = r->read16(r);
			font->kerning[i].y = r->read16(r);
			font->kerning[i].ax = reader_get_32(r);
		}
	}
	font->pixel_scale = reader_get_32(r);
	font->data_length = reader_get_32(r);
	if (font->data_length > 0) {
		font->data = malloc(font->data_length);
		if (font->data == nullptr) {
			free(font->kerning);
			free(font->rects);
			free(font);
			return nullptr;
		}
		reader_get_bytes(r, font->data, font->data_length);
	} else
		font->data = nullptr;
	font->ascent = reader_get_32(r);
	font->descent = reader_get_32(r);
	font->style = reader_get_32(r);
	font->num_names = reader_get_32(r);
	if (font->num_names > 0) {
		font->names = calloc(font->num_names, sizeof(*font->names));
		if (font->names == nullptr) {
			free(font->kerning);
			free(font->rects);
			free(font);
			return nullptr;
		}

		for (uint32_t i = 0; i < font->num_names; i++) {
			font->names[i].length = reader_get_32(r);
			reader_get_string(r, font->names[i].name, font->names[i].length);
			reader_align(r, 4);
		}
	}
	font->num_fallbacks = reader_get_32(r);
	if (font->num_fallbacks > 0) {
		font->fallbacks = calloc(font->num_fallbacks, sizeof(*font->fallbacks));
		if (font->fallbacks == nullptr) {
			free(font->names);
			free(font->rects);
			free(font->kerning);
			free(font);
			return nullptr;
		}

		for (uint32_t i = 0; i < font->num_fallbacks; i++) {
			font->fallbacks[i].file_id = reader_get_32(r);
			font->fallbacks[i].path_id = reader_get_64(r);
		}
	}
	font->rendering_mode = reader_get_32(r);
	font->round_adv_value = reader_get_8(r);

	return font;
}

void *pack_font_asset(const struct font_asset *font, uint32_t *size)
{
	struct writer w;

	(void)size;

	writer_set_write_memory(&w);

	/*if (font->char_rect_size != 0) {
	    for (uint32_t i = 0; i < font->char_rect_size; i++) {
	    writer_put_le32(&w, font->rects[i].index);
	    writer_put_le32(&w, font->rects[i].uv.x);
	    writer_put_le32(&w, font->rects[i].uv.y);
	    writer_put_le32(&w, font->rects[i].uv.w);
	    writer_put_le32(&w, font->rects[i].uv.h);
	    writer_put_le32(&w, font->rects[i].advice);
	    writer_put_8(&w, font->rects[i].flipped);
	    writer_align(&w, 4);
	    }
	}
	new_size += sizeof(font->kerning_size);
	if (font->kerning_size > 0)
	    new_size += font->kerning_size * 8;
	new_size += sizeof(font->pixel_scale);
	new_size += sizeof(font->data_length);
	new_size += font->data_length;
	new_size += sizeof(font->ascent);
	new_size += sizeof(font->descent);
	new_size += sizeof(font->style);
	new_size += sizeof(font->num_names);
	for (uint32_t i = 0; i < font->num_names; i++) {
	    new_size += sizeof(font->names[i].length);
	    new_size += font->names[i].length;
	    new_size += align(new_size, 4);
	}
	new_size += sizeof(font->num_fallbacks);
	if (font->num_fallbacks > 0)
	    new_size += font->num_fallbacks * sizeof(pptr_t);
	new_size += sizeof(font->rendering_mode);
	new_size += sizeof(font->round_adv_value);*/

	writer_put_le32(&w, font->title_length);
	writer_put_bytes(&w, font->title, font->title_length);
	writer_align(&w, 4);
	writer_put_le32(&w, font->line_spacing);
	writer_put_le32(&w, font->material.file_id);
	writer_put_le64(&w, font->material.path_id);
	writer_put_le32(&w, font->font_size);
	writer_put_le32(&w, font->texture.file_id);
	writer_put_le64(&w, font->texture.path_id);
	writer_put_le32(&w, font->ascii_offset);
	writer_put_le32(&w, font->tracking);
	writer_put_le32(&w, font->char_spacing);
	writer_put_le32(&w, font->char_padding);
	writer_put_le32(&w, font->convert_case);
	writer_put_le32(&w, font->char_rect_size);
	for (uint32_t i = 0; i < font->char_rect_size; i++) {
		writer_put_le32(&w, font->rects[i].index);
		writer_put_le32(&w, font->rects[i].uv.x);
		writer_put_le32(&w, font->rects[i].uv.y);
		writer_put_le32(&w, font->rects[i].uv.w);
		writer_put_le32(&w, font->rects[i].uv.h);
		writer_put_le32(&w, font->rects[i].advice);
		writer_put_8(&w, font->rects[i].flipped);
		writer_align(&w, 4);
	}
	writer_put_le32(&w, font->kerning_size);
	for (uint32_t i = 0; i < font->kerning_size; i++) {
		writer_put_le16(&w, font->kerning[i].x);
		writer_put_le16(&w, font->kerning[i].y);
		writer_put_le32(&w, font->kerning[i].ax);
	}
	writer_put_le32(&w, font->pixel_scale);
	writer_put_le32(&w, font->data_length);
	if (font->data_length > 0) {
		writer_put_bytes(&w, font->data, font->data_length);
		writer_align(&w, 4); /* questionable */
	}
	writer_put_le32(&w, font->ascent);
	writer_put_le32(&w, font->descent);
	writer_put_le32(&w, font->style);
	writer_put_le32(&w, font->num_names);
	if (font->num_names > 0)
		for (uint32_t i = 0; i < font->num_names; i++) {
			writer_put_le32(&w, font->names[i].length);
			writer_put_bytes(&w, font->names[i].name, font->names[i].length);
			writer_align(&w, 4);
		}
	writer_put_le32(&w, font->num_fallbacks);
	if (font->num_fallbacks > 0)
		for (uint32_t i = 0; i < font->num_fallbacks; i++) {
			writer_put_le32(&w, font->fallbacks[i].file_id);
			writer_put_le64(&w, font->fallbacks[i].path_id);
		}
	writer_put_le32(&w, font->rendering_mode);
	writer_put_8(&w, font->round_adv_value);

	return nullptr;
}

void destroy_font_asset(font_asset *font)
{
	if (font) {
		if (font->char_rect_size > 0)
			free(font->rects);

		if (font->kerning_size > 0)
			free(font->kerning);

		if (font->num_names > 0)
			free(font->names);

		if (font->num_fallbacks > 0)
			free(font->fallbacks);
		if (font->data != nullptr)
			free(font->data);
	}
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
