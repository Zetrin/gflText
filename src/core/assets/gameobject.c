#include "gameobject.h"

#include <malloc.h>
#include <string.h>

gameobject_asset_t *parse_gameobject_asset(struct reader *r)
{
	gameobject_asset_t *gameobject = malloc(sizeof(*gameobject));
	if (gameobject == nullptr) {
		perror("Could not allocate memory for gameobject");
		return nullptr;
	}

	memset(gameobject, 0, sizeof(*gameobject));

	gameobject->title_length = reader_get_le32(r);
	if (gameobject->title_length > 0) {
		/* gameobject->title = (char*)data + offset;*/
		/* offset += gameobject->title_length; */
		reader_align(r, 4);
		/* @TODO */
	}

	return gameobject;
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
