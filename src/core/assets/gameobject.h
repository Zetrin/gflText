#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <stddef.h>
#include <stdint.h>

#include "../io/reader.h"
#include "pptr_t.h"

typedef struct gameobject {
	uint32_t count;
	pptr_t *pointers;
	/* padding */
	uint32_t title_length;
	char *title;
} gameobject_asset_t;

gameobject_asset_t *parse_gameobject_asset(struct reader *r);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
