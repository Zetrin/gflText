#ifndef SPRITE_H
#define SPRITE_H

#include <stddef.h>
#include <stdint.h>

struct rect {
	float x;
	float y;
	float width;
	float height;
};

struct point {
	float x;
	float y;
};

struct sprite {
	char *title;
	uint32_t title_length;
	uint32_t width;
	uint32_t height;
	uint32_t size;
	struct rect rect;
	struct point offset;
	struct rect border;
	float pix2units;
	struct point pivot;
	uint32_t extrude;
	uint8_t is_polygon;
	/* padding align(4) */
	char guid[16];
	uint64_t second;
	char *atlas_tags;
};

typedef struct rect rect_t;
typedef struct sprite sprite_asset_t;

sprite_asset_t *parse_sprite_asset(void *data, size_t size);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
