#include "text.h"

#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include "../io/reader.h"
#include "../io/writer.h"

struct text_asset *parse_text_asset(struct reader *r)
{
	struct text_asset *text = malloc(sizeof(*text));
	if (text == nullptr) {
		perror("Could not allocate memory for text");
		return nullptr;
	}

	memset(text, 0, sizeof(*text));

	text->title_length = reader_get_32(r);
	reader_get_bytes(r, text->title, text->title_length);
	reader_align(r, 4);

	text->body_length = reader_get_le32(r);
	text->body = malloc(text->body_length);

	if (text->body == nullptr)
		return nullptr;

	reader_get_bytes(r, text->body, text->body_length);

	return text;
}

void destroy_text_asset(struct text_asset *text)
{
	free(text->body);
	free(text);
}

void *pack_text_asset(const struct text_asset *text, uint32_t *size)
{
	struct writer w;

	writer_set_write_memory(&w);

	// @TODO ENDIANESS

	writer_put_le32(&w, text->title_length);
	writer_put_bytes(&w, text->title, text->title_length);
	writer_align(&w, 4);
	writer_put_le32(&w, text->body_length);
	writer_put_bytes(&w, text->body, text->body_length);

	*size = w.size;

	return w.data;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
