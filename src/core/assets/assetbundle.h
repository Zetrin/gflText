#ifndef ASSETBUNDLE_H
#define ASSETBUNDLE_H

#include <stddef.h>
#include <stdint.h>

#include "../io/reader.h"
#include "../serialized.h"
#include "pptr_t.h"

struct pptr_table {
	uint32_t c;
	pptr_t *v;
};

struct container_info {
	uint32_t filepath_len;
	char *filepath;
	uint32_t preload_index;
	uint32_t preload_lenght;
	uint32_t file_id;
	uint64_t path_id;
};

struct container_info_table {
	uint32_t c;
	struct container_info *v;
};

struct assetbundle_asset {
	uint32_t bundle_name_len;
	char bundle_name[64];
	struct pptr_table pointers;
	struct container_info_table containers;
	// uint64_t unk1; =0
	// uint64_t unk2; =0
	// uint32_t unk3; =0
	// uint32_t some_count; =1
	// uint32_t bundle_name_len (2);
	// char bundle_name (2);
	// uint64_t unk4; =0
	// uint32_t unk5; =0
	// uint64_t unk6; =7
};

struct assetbundle_asset *assetbundle_parse(struct reader *r);
void *assetbundle_pack(struct assetbundle_asset *bundle, uint32_t *size);
void assetbundle_free(struct assetbundle_asset *bundle);
void assetbundle_link_containers(const struct assetbundle_asset *bundle, struct asset_array assets);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
