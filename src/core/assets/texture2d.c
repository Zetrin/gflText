#define _GNU_SOURCE
#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>

#include "texture2d.h"

texture2d_asset *parse_texture2d_asset(struct reader *r)
{
	texture2d_asset *t2d = malloc(sizeof(*t2d));
	if (t2d == nullptr) {
		perror("Could not allocate memory for audioclip");
		return nullptr;
	}

	memset(t2d, 0, sizeof(*t2d));

	t2d->title_length = reader_get_32(r);
	reader_get_bytes(r, t2d->title, t2d->title_length);
	reader_align(r, 4);

	const uint32_t bytes_to_skip = reader_get_32(r);
	reader_skip(r, bytes_to_skip);

	t2d->width = reader_get_32(r);
	t2d->height = reader_get_32(r);
	t2d->size = reader_get_32(r);
	t2d->format = reader_get_32(r);
	t2d->mip = reader_get_32(r);
	t2d->readable = reader_get_8(r);
	t2d->streaming_mipmaps = reader_get_8(r);
	reader_align(r, 4);

	t2d->streaming_mipmaps_priority = reader_get_32(r);
	t2d->images = reader_get_32(r);
	t2d->dimension = reader_get_32(r);
	t2d->settings.filter = reader_get_32(r);
	t2d->settings.aniso = reader_get_32(r);
	t2d->settings.mip_bias = reader_get_32(r);
	t2d->settings.wrap = reader_get_32(r);
	t2d->settings.wrap_v = reader_get_32(r);
	t2d->settings.wrap_w = reader_get_32(r);
	t2d->lightmap_format = reader_get_32(r);
	t2d->color_space = reader_get_32(r);
	t2d->data_size = reader_get_32(r);
	reader_align(r, 4);

	if (t2d->data_size == 0) {
		const uint32_t offset = reader_get_32(r);
		t2d->data_size = reader_get_32(r);

		reader_seek(r, offset);
	}

	t2d->data = malloc(t2d->data_size);
	if (t2d->data == nullptr) {
		perror("Can not allocate memory for texture data");
		return nullptr;
	}

	reader_get_bytes(r, t2d->data, t2d->data_size);

	return t2d;
}

void destroy_texture2d_asset(struct texture2d_asset *t2d)
{
	free(t2d->data);
	free(t2d);
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
