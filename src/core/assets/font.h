#ifndef FONT_H
#define FONT_H

#include <stddef.h>
#include <stdint.h>

#include "../io/reader.h"
#include "pptr_t.h"

typedef enum font_style {
	NORMAL = 0,
	BOLD = 1,
	ITALIC = 2,
	BOLD_ITALIC = 3
} font_style_t;

typedef struct font_name {
	uint32_t length;
	char *name;
} font_name;

typedef struct font_char_rect {
	uint32_t index;
	struct uv {
		float x;
		float y;
		float w;
		float h;
	} uv;
	struct vert {
		int32_t x;
		int32_t y;
		int32_t w;
		int32_t h;
	} vert;
	uint32_t advice;
	bool flipped;
} font_char_rect;

typedef struct font_kerning {
	uint16_t x;
	uint16_t y;
	float ax;
} font_kerning;

typedef struct font_asset {
	char title[64];
	uint32_t title_length;
	float line_spacing;
	pptr_t material;
	float font_size;
	pptr_t texture;
	uint32_t ascii_offset;
	float tracking;
	int32_t char_spacing;
	int32_t char_padding;
	int32_t convert_case;
	uint32_t char_rect_size;
	struct font_char_rect *rects;
	uint32_t kerning_size;
	struct font_kerning *kerning;
	float pixel_scale;
	uint32_t data_length;
	uint8_t *data;
	float ascent;
	float descent;
	font_style_t style;
	uint32_t num_names;
	struct font_name *names;
	uint32_t num_fallbacks;
	pptr_t *fallbacks;
	uint32_t rendering_mode;
	bool round_adv_value;
} font_asset;

struct font_asset *parse_font_asset(struct reader *r);
void *pack_font_asset(const struct font_asset *font, uint32_t *size);
void destroy_font_asset(struct font_asset *font);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
