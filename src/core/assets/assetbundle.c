#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../common.h"
#include "assetbundle.h"

struct container {
	uint32_t file_id;
	uint64_t path_id;
	char *name;
	uint32_t lenght;
};

void assetbundle_link_containers(const struct assetbundle_asset *bundle, struct asset_array assets)
{
	struct container *containers = nullptr;

	if (bundle == nullptr)
		return;

	containers = calloc(bundle->pointers.c, sizeof(*containers));
	if (containers == nullptr) {
		perror("Can not allocate memory for containers");
		return;
	}

	for (uint32_t i = 0; i < bundle->containers.c; i++) {
		const uint32_t start = bundle->containers.v[i].preload_index;
		const uint32_t end = start + bundle->containers.v[i].preload_lenght;

		for (uint32_t k = start; k < end; k++) {
			containers[k].lenght = bundle->containers.v[i].filepath_len;
			containers[k].name = bundle->containers.v[i].filepath;
			containers[k].file_id = bundle->containers.v[i].file_id;
			containers[k].path_id = bundle->containers.v[i].path_id;
		}
	}

	for (uint32_t i = 0; i < assets.c; i++) {
		for (uint32_t j = 0; j < bundle->pointers.c; j++) {
			if (bundle->pointers.v[j].path_id == assets.v[i].path_id) {
				assets.v[i].container = strndup(containers[j].name, containers[j].lenght);
				break;
			}
		}
	}

	free(containers);
}

struct assetbundle_asset *assetbundle_parse(struct reader *r)
{
	struct assetbundle_asset *bundle = malloc(sizeof(*bundle));
	if (bundle == nullptr) {
		perror("Error: can not allocate memory for assetbundle type");
		return nullptr;
	}
	memset(bundle, 0, sizeof(*bundle));

	bundle->bundle_name_len = reader_get_32(r);
	reader_get_bytes(r, bundle->bundle_name, bundle->bundle_name_len);
	reader_align(r, 4);

	bundle->pointers.c = reader_get_32(r);
	bundle->pointers.v = calloc(bundle->pointers.c, sizeof(*bundle->pointers.v));
	if (bundle->pointers.v == nullptr) {
		perror("Error: can not allocate memory for pointers table");
		free(bundle);
		return nullptr;
	}

	for (uint32_t i = 0; i < bundle->pointers.c; i++) {
		bundle->pointers.v[i].file_id = reader_get_32(r);
		bundle->pointers.v[i].path_id = reader_get_64(r);
	}

	bundle->containers.c = reader_get_32(r);
	bundle->containers.v = calloc(bundle->containers.c, sizeof(*bundle->containers.v));
	if (bundle->containers.v == nullptr) {
		perror("Error: can not allocate memory for containers table");
		free(bundle->pointers.v);
		free(bundle);
		return nullptr;
	}

	for (uint32_t i = 0; i < bundle->containers.c; i++) {
		bundle->containers.v[i].filepath_len = reader_get_32(r);

		// char *name = malloc(bundle->containers.v[i].filepath_len + 1);
		bundle->containers.v[i].filepath = malloc(bundle->containers.v[i].filepath_len + 1);
		if (bundle->containers.v[i].filepath == nullptr) {
			for (uint32_t j = 0; j < i; j++)
				free(bundle->containers.v[j].filepath);
			free(bundle->containers.v);
			free(bundle->pointers.v);
			free(bundle);
			return nullptr;
		}

		reader_get_bytes(r, bundle->containers.v[i].filepath, bundle->containers.v[i].filepath_len);
		bundle->containers.v[i].filepath[bundle->containers.v[i].filepath_len] = 0;
		reader_align(r, 4);
		// bundle->containers.v[i].filepath  = name;
		bundle->containers.v[i].preload_index = reader_get_32(r);
		bundle->containers.v[i].preload_lenght = reader_get_32(r);
		bundle->containers.v[i].file_id = reader_get_32(r);
		bundle->containers.v[i].path_id = reader_get_64(r);
	}

	return bundle;
}

void *assetbundle_pack(struct assetbundle_asset *bundle, uint32_t *size)
{
	(void)bundle;
	(void)size;
	return nullptr;
}

void assetbundle_free(struct assetbundle_asset *bundle)
{

	if (unlikely(bundle == nullptr))
		return;

	if (likely(bundle->pointers.c > 0)) {
		free(bundle->pointers.v);
		bundle->pointers.c = 0;
	}

	if (likely(bundle->containers.c > 0)) {
		for (uint32_t i = 0; i < bundle->containers.c; i++)
			free(bundle->containers.v[i].filepath);
		free(bundle->containers.v);
		bundle->containers.c = 0;
	}

	free(bundle);
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
