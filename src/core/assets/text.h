#ifndef TEXT_H
#define TEXT_H

#include <stddef.h>
#include <stdint.h>

#include "../io/reader.h"

typedef struct text_asset {
	char title[32];
	uint32_t title_length;
	uint8_t *body;
	uint32_t body_length;
} text_asset;

struct text_asset *parse_text_asset(struct reader *r);
void destroy_text_asset(struct text_asset *text);

void *pack_text_asset(const struct text_asset *text, uint32_t *size);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
