#include "audioclip.h"

#include <malloc.h>
#include <string.h>

struct audioclip *parse_audioclip_asset(struct reader *r)
{
	struct audioclip *clip = malloc(sizeof(struct audioclip));
	if (clip == nullptr) {
		perror("Could not allocate memory for audioclip");
		return nullptr;
	}

	memset(clip, 0, sizeof(struct audioclip));

	clip->title_length = reader_get_32(r);
	clip->title = malloc(clip->title_length);
	reader_get_string(r, clip->title, clip->title_length);
	reader_align(r, 4);

	clip->load_type = reader_get_32(r);
	clip->channels = reader_get_32(r);
	clip->frequency = reader_get_32(r);
	clip->bps = reader_get_32(r);
	clip->length = reader_get_32(r);
	clip->tracker = reader_get_8(r);
	reader_align(r, 4);
	clip->subsound_index = reader_get_32(r);
	clip->preload_audio_data = reader_get_8(r);
	clip->load_in_background = reader_get_8(r);
	clip->legacy_3D = reader_get_8(r);
	reader_align(r, 4);
	const uint32_t source_size = reader_get_32(r);
	clip->source = malloc(source_size);
	reader_get_bytes(r, clip->source, source_size);
	reader_align(r, 4);
	clip->offset = reader_get_64(r);
	clip->size = reader_get_64(r);
	clip->compression_format = r->read32(r);

	uint32_t tmp = reader_get_32(r);
	while (tmp != 0x35425346)
		tmp = reader_get_32(r);
	/* clip->data = (void*)((uintptr_t)data + offset + clip->offset); @TODO hex */

	return clip;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
