#ifndef AUDIOCLIP_H
#define AUDIOCLIP_H

#include <stddef.h>
#include <stdint.h>

#include "../io/reader.h"

struct audioclip {
	uint32_t title_length;
	char *title;

	uint32_t format;
	uint32_t type;
	bool use_3D;
	bool use_hardware;

	/* version 5 */
	uint32_t load_type;
	uint32_t channels;
	uint32_t frequency;
	uint32_t bps;
	float length;
	bool tracker;
	uint32_t subsound_index;
	bool preload_audio_data;
	bool load_in_background;
	bool legacy_3D;
	uint32_t compression_format;

	char *source;
	uint64_t offset;
	uint64_t size;
	void *data;
};

struct audioclip *parse_audioclip_asset(struct reader *r);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
