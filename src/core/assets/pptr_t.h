#ifndef PPTR_T_H
#define PPTR_T_H

#include <stdint.h>

typedef struct pptr {
	uint32_t file_id;
	uint64_t path_id;
} __attribute__((packed)) pptr_t;

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
