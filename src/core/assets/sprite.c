#include <stdlib.h>
#include <string.h>

#include "../io/reader.h"
#include "sprite.h"

sprite_asset_t *parse_sprite_asset(void *data, size_t size)
{
	off_t offset = 0;
	sprite_asset_t *sprite = nullptr;
	struct reader r;

	reader_set_read_from_memory(&r, data, size);

	sprite = malloc(sizeof(sprite_asset_t));
	if (sprite == nullptr) {
		perror("Could not allocate memory for audioclip");
		return nullptr;
	}

	memset(sprite, 0, sizeof(sprite_asset_t));
	sprite->title_length = read_int32le(&r);
	read_string(&r, sprite->title, sprite->title_length); /* @TODO */
	reader_align(&r, 4);

	sprite->rect.x = read_int32le(&r);
	sprite->rect.y = read_int32le(&r);
	sprite->rect.width = read_int32le(&r);
	sprite->rect.height = read_int32le(&r);

	sprite->offset.x = read_int32le(&r);
	sprite->offset.y = read_int32le(&r);

	sprite->border.x = read_int32le(&r);
	sprite->border.y = read_int32le(&r);
	sprite->border.width = read_int32le(&r);
	sprite->border.height = read_int32le(&r);

	sprite->pix2units = read_int32le(&r);

	sprite->pivot.x = read_int32le(&r);
	sprite->pivot.y = read_int32le(&r);

	sprite->extrude = read_int32le(&r);

	sprite->is_polygon = read_byte(&r);
	reader_align(&r, 4);

	read_bytes(&r, sprite->guid, 16);
	sprite->second = read_int64le(&r);

	uint32_t tags_len = read_int32le(&r);
	if (tags_len > 0) {
		sprite->atlas_tags = data + offset;
		/* @TODO */
		printf("Detected sprite with non-empty atlas tags\n");
	}

	return sprite;
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
