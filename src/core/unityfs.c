#include <lz4.h>
#include <lz4hc.h>
#include <lzma.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/cdefs.h>

#include "common.h"
#include "io/reader.h"
#include "io/writer.h"
#include "unityfs.h"

#define UNITYFS "UnityFS\0"

#define MAX_BLOCK_SIZE      131072U
#define BUFFER_OPTIMAL_SIZE LZ4_COMPRESSBOUND(MAX_BLOCK_SIZE)

static void reset(unityfs *uf)
{
	memset(uf, 0, sizeof(*uf));
}

static bool parse_blocks_dirs_info(unityfs *uf, void *data, const size_t size)
{
	struct dir_info *dirs = nullptr;
	struct block_info *blocks = nullptr;
	struct reader r;
	char hash[16];

	reader_set_read_memory(&r, data, size);

	reader_get_bytes(&r, hash, sizeof(hash));

	const uint32_t blocks_count = reader_get_be32(&r);
	if (likely(blocks_count > 0)) {
		blocks = calloc(blocks_count, sizeof(*blocks));
		if (unlikely(blocks == nullptr)) {
			perror("Error: can not allocate memory to unpack directory metadata");
			return false;
		}

		for (uint32_t i = 0; i < blocks_count; i++) {
			blocks[i].uncompressed_size = reader_get_be32(&r);
			blocks[i].compressed_size = reader_get_be32(&r);
			blocks[i].flags = reader_get_be16(&r);
		}
	}

	const uint32_t dirs_count = reader_get_be32(&r);
	if (likely(dirs_count > 0)) {
		dirs = calloc(dirs_count, sizeof(*dirs));
		if (unlikely(dirs == nullptr)) {
			perror("Error: can not allocate memory to unpack directory info");
			free(blocks);
			return false;
		}

		for (uint32_t i = 0; i < dirs_count; i++) {
			dirs[i].offset = reader_get_be64(&r);
			dirs[i].size = reader_get_be64(&r);
			dirs[i].flags = reader_get_be32(&r);
			reader_get_string(&r, dirs[i].cab_id, sizeof(dirs[i].cab_id));
		}
	}

	uf->blocks.c = blocks_count;
	uf->blocks.v = blocks;
	uf->dirs.c = dirs_count;
	uf->dirs.v = dirs;

	return true;
}

static bool bundle_parse_blocks_dirs_lz4(struct unityfs *uf, const void *cinfo, const uint32_t csz, const uint32_t ucsz)
{
	void *dirbuf = malloc(ucsz);
	if (unlikely(dirbuf == nullptr)) {
		perror("Error: can not allocate memory to unpack metadata");
		return false;
	}

	const int ret = LZ4_decompress_safe(cinfo, dirbuf, csz, ucsz);
	if (unlikely(ret < 0)) {
		free(dirbuf);
		fprintf(stderr, "Failed to decompress metadata: output buffer too small or data corrupted.\n");
		return false;
	}

	if (unlikely((uint32_t)ret != ucsz)) {
		fprintf(stderr, "Failed to decompress metadata: expected %u bytes, got %u bytes\n", ucsz, ret);
		free(dirbuf);
		return false;
	}

	const bool parsed = parse_blocks_dirs_info(uf, dirbuf, ucsz);

	free(dirbuf);

	return parsed;
}

bool bundle_read_header_and_blocks(struct unityfs *uf)
{
	char magic[8] = {0};

	if (setjmp(uf->reader->jmp_buf))
		return false;

	reader_get_string(uf->reader, magic, sizeof(magic));

	if (uf->reader->size <= 16)
		return false;

	if (memcmp(magic, UNITYFS, sizeof(magic)) != 0) {
		fprintf(stderr, "Not a UnityFS\n");
		return false;
	}

	reader_align(uf->reader, 2);
	uf->version = reader_get_be32(uf->reader);
	reader_get_string(uf->reader, uf->u_version, sizeof(uf->u_version));
	reader_align(uf->reader, 2);
	reader_get_string(uf->reader, uf->u_revision, sizeof(uf->u_revision));
	reader_align(uf->reader, 2);
	reader_get_be64(uf->reader); // file_size
	uint32_t csz = reader_get_be32(uf->reader);
	uint32_t ucsz = reader_get_be32(uf->reader);
	uf->flags = reader_get_be32(uf->reader);

	if (uf->version >= 7)
		reader_align(uf->reader, 16);

	const compression_type_t ct = uf->flags & ARCHIVE_COMPRESSION_MASK;

	void *cinfo = malloc(csz);
	if (unlikely(cinfo == nullptr)) {
		perror("Error: can not allocate memory to unpack metadata");
		return false;
	}

	if (setjmp(uf->reader->jmp_buf)) {
		free(cinfo);
		return false;
	}

	if (uf->flags & ARCHIVE_INFO_AT_END) {
		const uint64_t info_offset = uf->reader->size - csz;
		const uint64_t saved_offset = reader_tell(uf->reader);
		reader_seek(uf->reader, info_offset);
		reader_get_bytes(uf->reader, cinfo, csz);
		reader_seek(uf->reader, saved_offset);
	} else {
		reader_get_bytes(uf->reader, cinfo, csz);
	}

	uf->blocks.offset = reader_tell(uf->reader);

	bool parsed = false;
	switch (ct) {
		case COMPRESSION_LZ4:
		case COMPRESSION_LZ4HC:
			parsed = bundle_parse_blocks_dirs_lz4(uf, cinfo, csz, ucsz);
			break;
		case COMPRESSION_LZMA:
		case COMPRESSION_LZHAM:
		case COMPRESSION_NONE:
		default:
			fprintf(stderr, "Unsupported compression\n");
	}

	free(cinfo);

	if (uf->flags & ARCHIVE_PADDING_AT_START)
		reader_align(uf->reader, 16);

	return parsed;
}

static bool bundle_unpack_blocks(struct block_list *blocks, struct reader *r, struct writer *w)
{
	char *src = nullptr, *dst = nullptr;

	if (unlikely(blocks == nullptr))
		return false;

	if (unlikely(blocks->c == 0))
		return false;

	if (r == nullptr || w == nullptr)
		return false;

	reader_seek(r, blocks->offset);

	src = malloc(MAX_BLOCK_SIZE);
	if (unlikely(src == nullptr)) {
		perror("Error: can not allocate memory for unpacking");
		return false;
	}

	dst = malloc(MAX_BLOCK_SIZE);
	if (unlikely(dst == nullptr)) {
		free(src);
		perror("Error: can not allocate memory for unpacking");
		return false;
	}

	for (uint32_t i = 0; i < blocks->c; i++) {
		const uint32_t csz = blocks->v[i].compressed_size;
		const uint32_t ucsz = blocks->v[i].uncompressed_size;
		const compression_type_t ct = blocks->v[i].flags & ARCHIVE_COMPRESSION_MASK;

		reader_get_bytes(r, src, csz);

		switch (ct) {
			case COMPRESSION_LZ4:
			case COMPRESSION_LZ4HC:
				int32_t ret = LZ4_decompress_safe(src, dst, csz, ucsz);
				if (ret < 0) {
					fprintf(stderr, "Decompression failed at block %u: buffer too small or data corrupted\n", i);
					free(src);
					free(dst);
					return false;
				}
				if ((uint32_t)ret != blocks->v[i].uncompressed_size) {
					fprintf(stderr, "Decompression failed at block %u: expected %u, got %i bytes\n", i, ucsz, ret);
					free(src);
					free(dst);
					return false;
				}
				break;

			case COMPRESSION_NONE:
				writer_put_bytes(w, src, csz);
				continue;
			default:
				fprintf(stderr, "Unsupported block compression\n");
				free(dst);
				return false;
		}

		writer_put_bytes(w, dst, ucsz);
	}

	free(src);
	free(dst);

	return true;
}

bool bundle_unpack(unityfs *uf)
{
	if (uf->reader == nullptr)
		return false;

	if (uf->writer == nullptr) {
		uf->writer = writer_create_memory_writer();
		if (uf->writer == nullptr)
			return false;
	}

	return bundle_unpack_blocks(&uf->blocks, uf->reader, uf->writer);
}

void bundle_dump(const unityfs *const uf)
{
	struct writer *w = uf->writer;

	// uint64_t serialized_size = 0;
	// for (uint32_t i = 0; i < uf->blocks.c; i++)
	//	serialized_size += uf->blocks.v[i].uncompressed_size;

	writer_put_bytes(w, UNITYFS, 8);
	writer_put_32(w, uf->version);
	writer_put_bytes(w, uf->u_version, 6);
	writer_put_bytes(w, uf->u_revision, 12);
	writer_put_64(w, 0); // file size
	// writer_put_32be(w, uf->uncompressed);
	// writer_put_32be(w, uf->uncompressed);
	writer_put_32(w, uf->flags & 0xFFFFFFF0);
}

void bundle_free(struct unityfs *uf)
{
	if (uf->blocks.c > 0)
		free(uf->blocks.v);

	if (uf->dirs.c > 0)
		free(uf->dirs.v);

	reset(uf);
}

bool bundle_pack(struct unityfs *uf, char *sdata, const size_t ssize, size_t *out_size)
{
	(void)uf;
	(void)sdata;
	(void)ssize;
	(void)out_size;
	/*
	const uint32_t blocks_count = ssize / MAX_BLOCK_SIZE + ((ssize % MAX_BLOCK_SIZE) > 0);
	void *header_data = nullptr;
	void *metadata_uncompressed = nullptr;
	void *metadata_compressed = nullptr;
	void *data = nullptr;
	char *new_bundle = nullptr;
	off_t soffset = 0;
	size_t header_size = 0;
	size_t data_size = 0;
	uint32_t metadata_uncompressed_size = 0;
	uint32_t metadata_compressed_size = 0;
	size_t new_bundle_size = 0;
	void* state = nullptr;
	int to_read = MAX_BLOCK_SIZE;
	off_t data_offset = 0;
	struct writer w; // => full file
	struct writer hdr_w; // => header
	struct writer meta_w; // => metadata_uncompressed

	header_size += strlen(uf->magic) + 1;
	header_size += align(header_size, 2);
	header_size += sizeof(uf->version);
	header_size += strlen(uf->u_version) + 1;
	header_size += align(header_size, 2);
	header_size += strlen(uf->u_revision) + 1;
	header_size += align(header_size, 2);
	header_size += sizeof(uf->size);
	header_size += sizeof(metadata_compressed_size);
	header_size += sizeof(metadata_uncompressed_size);
	header_size += sizeof(uf->flags);

	metadata_uncompressed_size += sizeof(uf->hash);
	metadata_uncompressed_size += sizeof(uf->blocks.c);
	metadata_uncompressed_size += blocks_count * sizeof(struct block_info);
	metadata_uncompressed_size += sizeof(uf->dirs.c);
	for (uint32_t d = 0; d < uf->dirs.c; d++) {
	    metadata_uncompressed_size += sizeof(uf->dirs.v[d].offset);
	    metadata_uncompressed_size += sizeof(uf->dirs.v[d].size);
	    metadata_uncompressed_size += sizeof(uf->dirs.v[d].flags);
	    metadata_uncompressed_size += strlen(uf->dirs.v[0].cab_id) + 1;
	}

	metadata_uncompressed = malloc(metadata_uncompressed_size);
	if(metadata_uncompressed == nullptr) {
	    perror("Error: can not allocate memory for uncomressed metadata");
	    goto fail;
	}

	write_bytes(&meta_w, uf->hash, sizeof(uf->hash));
	write_int32be(&meta_w, blocks_count);

	state = malloc(LZ4_sizeofStateHC());
	if (state == nullptr) {
	    fprintf(stderr, "%s\n", "Error: can not allocate LZ4 state");
	    goto fail;
	}
	to_read = MAX_BLOCK_SIZE;

	while (soffset < (ssize_t)ssize) {
	    int csize = LZ4_compress_HC((char*)sdata + soffset, (char*)data + data_offset, to_read, MAX_BLOCK_SIZE, LZ4HC_CLEVEL_MAX);

	    if (csize == 0) {
	        fprintf(stderr, "%s\n", "Error: failed to compress data block");
	        goto fail;
	    }

	    write_int32be(&meta_w, to_read);
	    write_int32be(&meta_w, csize);
	    write_int16be(&meta_w, COMPRESSION_LZ4HC);

	    data_offset += csize;
	    soffset += to_read;
	    data_size += csize;

	    if (ssize - soffset < MAX_BLOCK_SIZE)
	        to_read = ssize - soffset;
	}

	free(state); state = nullptr;

	write_int32be(&meta_w, uf->dirs.c);
	write_int64be(&meta_w, uf->dirs.v[0].offset);
	write_int64be(&meta_w, ssize);
	write_int32be(&meta_w, uf->dirs.v[0].flags);
	write_string(&meta_w, uf->dirs.v[0].cab_id);

	metadata_compressed = malloc(metadata_uncompressed_size);
	if (metadata_compressed == nullptr) {
	    perror("Error: can not allocate memory for compressed metadata");
	    goto fail;
	}

	memset(metadata_compressed, 0, metadata_uncompressed_size);
	metadata_compressed_size = LZ4_compress_HC(metadata_uncompressed, metadata_compressed, metadata_uncompressed_size, metadata_uncompressed_size, LZ4HC_CLEVEL_MAX);
	if (metadata_compressed_size == 0) {
	    fprintf(stderr, "%s\n", "Error: failed to compress metadata");
	    goto fail;
	}

	free(metadata_uncompressed); metadata_uncompressed = nullptr;

	new_bundle_size += header_size;
	new_bundle_size += align(new_bundle_size, 2);
	new_bundle_size += metadata_compressed_size;
	new_bundle_size += data_size;

	header_data = malloc(header_size);
	if (header_data == nullptr) {
	    perror("Error: can not allocate memory for new bundle header");
	    goto fail;
	}

	write_string(&hdr_w, uf->magic);
	writer_align(&hdr_w, 2);
	write_int32be(&hdr_w, uf->version);
	write_string(&hdr_w, uf->u_version);
	write_string(&hdr_w, uf->u_revision);
	write_int64be(&hdr_w, new_bundle_size);
	write_int32be(&hdr_w, metadata_compressed_size);
	write_int32be(&hdr_w, metadata_uncompressed_size);
	write_int32be(&hdr_w, uf->flags);

	write_bytes(&w, header_data, header_size);
	writer_align(&w, 2);
	write_bytes(&w, metadata_compressed, metadata_compressed_size);
	write_bytes(&w, data, data_size);

	free(header_data);
	free(metadata_compressed);
	free(data);

	*out_size = new_bundle_size;
	return new_bundle;

fail:
	free(data);
	free(state);
	free(metadata_compressed);
	free(metadata_uncompressed);

	*out_size = 0;
	return false;
	*/
	return false;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
