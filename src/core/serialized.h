#ifndef SERIALIZED_H
#define SERIALIZED_H

#include <stddef.h>
#include <stdint.h>

#include "assettype.h"

typedef enum target_platform {
	TARGET_NONE = -2,
	TARGET_ANY = -1,
	TARGET_VALID = 1,
	TARGET_OSX = 2,
	TARGET_OSX_PPC = 3,
	TARGET_OSX_INTEL32 = 4,
	TARGET_WINDOWS32,
	TARGET_WEBPLAYER,
	TARGET_WEBPLAYER_STREAMED,
	TARGET_WII = 8,
	TARGET_IOS = 9,
	TARGET_PS3,
	TARGET_XBOX360,
	TARGET_BROADCOM = 12,
	TARGET_ANDROID = 13,
	TARGET_GLESEMU = 14,
	TARGET_GLES20EMU = 15,
	TARGET_NACL = 16,
	TARGET_LINUX32 = 17,
	TARGET_FLASHPLAYER = 18,
	TARGET_WINDOWS64 = 19,
	TARGET_WEBGL,
	TARGET_WSAPLAYER,
	TARGET_LINUX_64 = 24,
	TARGET_LINUX_ANY,
	TARGET_WP8PLAYER,
	TARGET_OSX_INTEL64,
	TARGET_BLACKBERRY,
	TARGET_TIZEN,
	TARGET_PSP2,
	TARGET_PS4,
	TARGET_PSM,
	TARGET_XBOXONE,
	TARGET_SAMSUNGTV,
	TARGET_N3DS,
	TARGET_WIIU,
	TARGET_TVOS,
	TARGET_SWITCH,
	TARGET_LUMIN,
	TARGET_STADIA,
	TARGET_CLOUD,
	TARGET_GAMECORE_XBOXSERIES,
	TARGET_GAMECORE_XBOXONE,
	TARGET_PS5,
	TARGET_LINUX_EMBEDDED,
	TARGET_QNX,
	TARGET_UNKNOWN = 9999
} target_platform_t;

struct node {
	uint16_t version;
	uint8_t level;
	uint8_t flags;
	uint32_t type_offset;
	uint32_t name_offset;
	int32_t byte_size;
	uint64_t ref_type_hash;
	uint32_t index;
	uint32_t metaflags;
};

struct node_array {
	uint32_t c;
	struct node *v;
};

struct type_deps {
	uint32_t c;
	uint32_t *v;
};

struct type {
	uint32_t type_id;
	classid_t class_id;
	uint8_t is_stripped;
	int16_t script_type_index;
	uint8_t type_hash[16];
	uint8_t script_hash[16];
	uint8_t script_id[16];
	uint32_t string_buffer_size;
	struct node_array nodes;
	char *string_buffer;
	struct type_deps deps;
};

struct type_array {
	uint32_t c;
	struct type *v;
};

struct entry {
	uint64_t path_id;
	uint32_t offset;
	uint32_t size;
	int32_t type_id;
} __attribute__((packed));

struct catalog_entry {
	uint32_t title_length;
	char *path;
	uint32_t preload_index;
	uint64_t flags;
	uint64_t path_id;
} __attribute__((packed));

struct asset {
	uint64_t path_id;
	uint32_t data_offset;
	uint32_t size;
	struct type *type;
	char *container;
};

struct asset_array {
	uint32_t c;
	struct asset *v;
};

struct script {
	uint32_t file_id;
	uint64_t path_id;
};

struct script_array {
	uint32_t c;
	struct script *v;
};

struct external {
	char guid[16];
	uint32_t type;
	char path[82];
};

struct external_array {
	uint32_t c;
	struct external *v;
};

struct serialized {
	uint64_t metadata_size;
	uint64_t file_size;
	uint32_t version;
	uint64_t data_offset;
	uint32_t flags;
	char u_version[16];
	enum target_platform target_platform;
	bool enable_type_tree;
	uint8_t endianess;
	uint32_t big_id_enabled;
	struct type_array types;
	struct asset_array assets;
	struct script_array scripts;
	struct external_array externals;
	struct assetbundle_asset *catalog;
	char *user_string;
	struct reader *reader;
	struct writer *writer;
};

struct serialized *serialized_from_memory(void *data, size_t size);
struct serialized *serialized_from_fd(int fd);
struct serialized *serialized_from_file(const char *pathname);
void *serialized_file_to_data(struct serialized *s, size_t *data_size);
void serialized_free(struct serialized *s);

void serialized_read_paths(struct serialized *s);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
