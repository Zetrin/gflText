#ifndef BUNDLEASSET_H
#define BUNDLEASSET_H

#include <stddef.h>
#include <stdint.h>

#include "io/reader.h"
#include "io/writer.h"

enum block_flags {
	BLOCK_COMPRESSION_MASK = 0x3F,
	BLOCK_STREAMED = 0x40
};

typedef enum archive_flags {
	ARCHIVE_COMPRESSION_MASK = 0x3F,
	ARCHIVE_COMBINED_INFO = 0x40,
	ARCHIVE_INFO_AT_END = 0x80,
	ARCHIVE_OLD_WEB_COMPAT = 0x100,
	ARCHIVE_PADDING_AT_START = 0x200,
} archive_flag_t;

typedef enum compression_type {
	COMPRESSION_NONE,
	COMPRESSION_LZMA,
	COMPRESSION_LZ4,
	COMPRESSION_LZ4HC,
	COMPRESSION_LZHAM
} compression_type_t;

struct dir_info {
	uint64_t offset;
	uint64_t size;
	uint32_t flags;
	char cab_id[64];
};

struct dir_list {
	uint32_t c;
	struct dir_info *v;
};

struct block_info {
	uint32_t uncompressed_size;
	uint32_t compressed_size;
	uint16_t flags;
} __attribute__((packed));

struct block_list {
	uint64_t offset;
	uint32_t c;
	struct block_info *v;
};

typedef struct unityfs {
	uint32_t version;
	char u_version[6];
	char u_revision[12];
	//	uint64_t file_size;
	//	uint32_t compressed;
	//	uint32_t uncompressed;
	uint32_t flags;
	//	char hash[16];
	struct dir_list dirs;
	struct block_list blocks;
	struct reader *reader;
	struct writer *writer;
} unityfs;

void bundle_init(unityfs *uf, struct reader *r, struct writer *w);
bool bundle_read_header_and_blocks(unityfs *uf);
bool bundle_open_file(unityfs *uf, const char *filename);
bool bundle_unpack(unityfs *uf);

/* bool bundle_pack(struct unityfs *uf); */
void bundle_free(unityfs *uf);
void *pack(unityfs *u, void *data, size_t size, size_t *out_size);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
