#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>

#if (__GNUC__ >= 3)
#define unlikely(cond) __builtin_expect((cond), 0)
#define likely(cond)   __builtin_expect((cond), 1)
#else
#define unlikely(cond) (cond)
#define likely(cond)   (cond)
#endif

extern bool verbose;
#define LOG(...) \
	if (verbose) \
	printf(__VA_ARGS__)

#ifdef DEBUG
#define DBG(...) printf(__VA_ARGS__)
#else
#define log_dbg(...) \
	do {             \
	} while (0)
#endif

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
