#include <crc32c/crc32c.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/cdefs.h>

#include "assets/assetbundle.h"
#include "assetstring.h"
#include "assettype.h"
#include "common.h"
#include "io/reader.h"
#include "serialized.h"

[[maybe_unused]]
static const char *get_assetname(const char *strings, const uint32_t offset)
{
	const bool is_valid_offset = (offset & 0x80000000) == 0;
	const uint32_t id = offset & 0x7FFFFFFF;

	if (is_valid_offset && strings)
		return strings + offset;

	return assetname(id);
}

static inline bool serialized_read_types(struct serialized *s)
{
	struct type_array types = {0};

	types.c = reader_get_32(s->reader);
	if (likely(types.c > 0)) {
		types.v = calloc(types.c, sizeof(*types.v));
		if (unlikely(types.v == nullptr)) {
			perror("Error: can not allocate memory for types");
			return false;
		}

		for (uint32_t t = 0; t < types.c; t++) {
			types.v[t].class_id = reader_get_32(s->reader);
			types.v[t].is_stripped = reader_get_8(s->reader);
			types.v[t].script_type_index = reader_get_16(s->reader);

			if (types.v[t].class_id == CLASSID_MONOBEHAVIOUR)
				reader_get_bytes(s->reader, types.v[t].script_id, sizeof(types.v[t].script_id));

			reader_get_bytes(s->reader, types.v[t].type_hash, sizeof(types.v[t].type_hash));

			if (likely(s->enable_type_tree)) {
				types.v[t].nodes.c = reader_get_32(s->reader);
				types.v[t].string_buffer_size = reader_get_32(s->reader);

				if (likely(types.v[t].nodes.c > 0)) {
					types.v[t].nodes.v = calloc(types.v[t].nodes.c, sizeof(*types.v[t].nodes.v));
					if (unlikely(types.v[t].nodes.v == nullptr)) {
						perror("Error: can not allocate memory for nodes");
						for (uint32_t x = 0; x < t; x++)
							free(types.v[x].nodes.v);
						free(types.v);
						return false;
					}
				}

				for (uint32_t n = 0; n < types.v[t].nodes.c; n++) {
					types.v[t].nodes.v[n].version = reader_get_16(s->reader);
					types.v[t].nodes.v[n].level = reader_get_8(s->reader);
					types.v[t].nodes.v[n].flags = reader_get_8(s->reader);
					types.v[t].nodes.v[n].type_offset = reader_get_32(s->reader);
					types.v[t].nodes.v[n].name_offset = reader_get_32(s->reader);
					types.v[t].nodes.v[n].byte_size = reader_get_32(s->reader);
					types.v[t].nodes.v[n].index = reader_get_32(s->reader);
					types.v[t].nodes.v[n].metaflags = reader_get_32(s->reader);
					if (s->version >= 19)
						types.v[t].nodes.v[n].ref_type_hash = reader_get_64(s->reader);
				}

				if (likely(types.v[t].string_buffer_size > 0)) {
					types.v[t].string_buffer = malloc(types.v[t].string_buffer_size);
					if (types.v[t].string_buffer)
						reader_get_bytes(s->reader, types.v[t].string_buffer, types.v[t].string_buffer_size);
				}

				/*for (uint32_t n = 0; n < types.v[t].nodes.c; n++) {
				    types.v[t].nodes.v[n].type = get_assetname(types.v[t].string_buffer, types.v[t].nodes.v[n].type_offset);
				    types.v[t].nodes.v[n].name = get_assetname(types.v[t].string_buffer, types.v[t].nodes.v[n].name_offset);
				}*/

				if (s->version >= 21) {
					types.v[t].deps.c = reader_get_32(s->reader);

					if (types.v[t].deps.c > 0) {
						types.v[t].deps.v = calloc(types.v[t].deps.c, sizeof(*types.v[t].deps.v));
						if (types.v[t].deps.v == nullptr)
							return false;
						for (uint32_t d = 0; d < types.v[t].deps.c; d++)
							types.v[t].deps.v[d] = reader_get_32(s->reader);
					}
				}
			}
		}

		s->types = types;
	}

	return true;
}

static inline bool serialized_read_assets(struct serialized *s)
{
	struct asset_array assets;

	assets.c = reader_get_32(s->reader);
	if (likely(assets.c > 0)) {
		assets.v = calloc(assets.c, sizeof(*assets.v));
		if (unlikely(assets.v == nullptr)) {
			perror("Error: can not allocate memory for serialized file");
			return false;
		}

		for (uint32_t e = 0; e < assets.c; e++) {
			reader_align(s->reader, 4);
			assets.v[e].path_id = reader_get_64(s->reader);
			const uint64_t entry_data_offset = reader_get_32(s->reader);
			assets.v[e].size = reader_get_32(s->reader);
			const uint32_t type_id = reader_get_32(s->reader);
			assets.v[e].type = &s->types.v[type_id];
			assets.v[e].data_offset = s->data_offset + entry_data_offset;

			if (assets.v[e].type->class_id == CLASSID_ASSETBUNDLE) {
				struct reader bundle_reader;
				if (reader_clone(s->reader, &bundle_reader, assets.v[e].data_offset, assets.v[e].size))
					s->catalog = assetbundle_parse(&bundle_reader);
			}
		}
	}

	s->assets = assets;

	return true;
}

static inline bool serialized_read_scripts(struct serialized *s)
{
	struct script_array scripts;

	scripts.c = reader_get_32(s->reader);
	if (unlikely(scripts.c > 0)) {
		scripts.v = calloc(scripts.c, sizeof(*scripts.v));
		if (unlikely(scripts.v == nullptr)) {
			perror("Error: can not allocate memory for scripts");
			return false;
		}

		for (uint32_t x = 0; x < scripts.c; x++) {
			scripts.v[x].file_id = reader_get_32(s->reader);
			scripts.v[x].path_id = reader_get_64(s->reader);
		}
	}

	s->scripts = scripts;

	return true;
}

static inline bool serialized_read_externals(struct serialized *s)
{
	s->externals.c = reader_get_32(s->reader);
	if (s->externals.c > 0) {
		s->externals.v = calloc(s->externals.c, sizeof(*s->externals.v));
		if (unlikely(s->externals.v == nullptr)) {
			perror("Error: can not allocate memory for external values");
			return false;
		}

		for (uint32_t ext = 0; ext < s->externals.c; ext++) {
			reader_skip(s->reader, 1); // always empty
			reader_get_bytes(s->reader, s->externals.v[ext].guid, sizeof(s->externals.v[ext].guid));
			s->externals.v[ext].type = reader_get_32(s->reader);
			reader_get_string(s->reader, s->externals.v[ext].path, sizeof(s->externals.v[ext].path));
			reader_align(s->reader, 4);
		}
	}

	return true;
}

[[maybe_unused]]
static inline bool serialized_read_refs(struct serialized *s)
{
	struct type_array refs;

	refs.c = reader_get_32(s->reader);

	if (likely(refs.c > 0)) {
		refs.v = calloc(refs.c, sizeof(*refs.v));
		if (unlikely(refs.v == nullptr)) {
			perror("Error: can not allocate memory for types");
			return false;
		}

		for (uint32_t t = 0; t < refs.c; t++) {
			refs.v[t].type_id = t;
			refs.v[t].class_id = reader_get_32(s->reader);
			refs.v[t].is_stripped = reader_get_8(s->reader);
			refs.v[t].script_type_index = reader_get_16(s->reader);
			if (refs.v[t].script_type_index >= 0)
				reader_get_bytes(s->reader, refs.v[t].script_id, sizeof(refs.v[t].script_id));
			else if (refs.v[t].class_id == CLASSID_MONOBEHAVIOUR)
				reader_get_bytes(s->reader, refs.v[t].script_id, sizeof(refs.v[t].script_id));
			reader_get_bytes(s->reader, refs.v[t].type_hash, sizeof(refs.v[t].type_hash));

			if (likely(s->enable_type_tree)) {
				refs.v[t].nodes.c = reader_get_32(s->reader);
				refs.v[t].string_buffer_size = reader_get_32(s->reader);
				// uint64_t strings_offset = reader_tell(s->reader) + refs[t].nodes.c * 24;

				if (likely(refs.v[t].nodes.c > 0)) {
					refs.v[t].nodes.v = calloc(refs.v[t].nodes.c, sizeof(*refs.v[t].nodes.v));
					if (unlikely(refs.v[t].nodes.v == nullptr)) {
						perror("Error: can not allocate memory for nodes");
						for (uint32_t x = 0; x < t; x++)
							free(refs.v[x].nodes.v);
						free(refs.v);
						return false;
					}
				}

				for (uint32_t n = 0; n < refs.v[t].nodes.c; n++) {
					refs.v[t].nodes.v[n].version = reader_get_16(s->reader);
					refs.v[t].nodes.v[n].level = reader_get_8(s->reader);
					refs.v[t].nodes.v[n].flags = reader_get_8(s->reader);
					refs.v[t].nodes.v[n].type_offset = reader_get_32(s->reader);
					refs.v[t].nodes.v[n].name_offset = reader_get_32(s->reader);
					refs.v[t].nodes.v[n].byte_size = reader_get_32(s->reader);
					refs.v[t].nodes.v[n].index = reader_get_32(s->reader);
					refs.v[t].nodes.v[n].metaflags = reader_get_32(s->reader);
					if (s->version >= 19)
						refs.v[t].nodes.v[n].ref_type_hash = reader_get_64(s->reader);
				}

				if (likely(refs.v[t].string_buffer_size > 0)) {
					refs.v[t].string_buffer = malloc(refs.v[t].string_buffer_size);
					if (refs.v[t].string_buffer)
						reader_get_bytes(s->reader, refs.v[t].string_buffer, refs.v[t].string_buffer_size);
				}

				if (s->version >= 21) {
					char info[256];
					size_t len;

					// refs.v[t].Klass
					len = reader_strlen(s->reader);
					if (len)
						reader_get_string(s->reader, info, len);

					// refs.v[t].Namespace
					len = reader_strlen(s->reader);
					if (len)
						reader_get_string(s->reader, info, len);

					// refs.v[t].Assembly
					len = reader_strlen(s->reader);
					if (len)
						reader_get_string(s->reader, info, len);
				}
			}
		}

		// s->refs = refs;
	}

	return true;
}

static struct serialized *serialized_file(struct reader *r)
{
	struct serialized *s;

	s = malloc(sizeof(*s));
	if (unlikely(s == nullptr)) {
		perror("Error: no memory for serialized file");
		return nullptr;
	}
	memset(s, 0, sizeof(*s));

	s->reader = r;
	s->metadata_size = reader_get_be32(s->reader);
	s->file_size = reader_get_be32(s->reader);
	s->version = reader_get_be32(s->reader);
	s->data_offset = reader_get_be32(s->reader);
	s->flags = reader_get_be32(s->reader);
	s->endianess = s->flags & 0x010000;

	if (unlikely(s->endianess))
		reader_set_big_endian(s->reader);
	else
		reader_set_little_endian(s->reader);

	reader_get_string(s->reader, s->u_version, sizeof(s->u_version));
	s->target_platform = reader_get_32(s->reader);
	s->enable_type_tree = reader_get_8(s->reader);

	if (!serialized_read_types(s)) {
		serialized_free(s);
		return nullptr;
	}

	if (!serialized_read_assets(s)) {
		serialized_free(s);
		return nullptr;
	}

	if (!serialized_read_scripts(s)) {
		serialized_free(s);
		return nullptr;
	}

	if (!serialized_read_externals(s)) {
		serialized_free(s);
		return nullptr;
	}

	/*if (s->version >= 20) {
	    if (!serialized_read_refs(s)) {
	        serialized_free(s);
	        return nullptr;
	    }
	}*/

	size_t len = reader_strlen(s->reader);
	if (unlikely(len)) {
		s->user_string = malloc(len + 1);
		reader_get_string(s->reader, s->user_string, len);
	}

	reader_align(s->reader, 16);

	return s;
}

struct serialized *serialized_from_file(const char *path)
{
	struct reader *r = reader_create_file_reader(path);
	if (unlikely(r == nullptr))
		return nullptr;

	return serialized_file(r);
}

struct serialized *serialized_from_memory(void *data, size_t size)
{
	struct reader *r = reader_create_memory_reader(data, size);
	if (unlikely(r == nullptr))
		return nullptr;

	return serialized_file(r);
}

void serialized_free(struct serialized *s)
{
	if (likely(s->types.c > 0)) {
		for (uint32_t i = 0; i < s->types.c; i++) {
			if (likely(s->types.v[i].nodes.c > 0))
				free(s->types.v[i].nodes.v);

			if (likely(s->types.v[i].string_buffer != nullptr))
				free(s->types.v[i].string_buffer);
		}

		free(s->types.v);
		s->types.c = 0;
		s->types.v = nullptr;
	}

	if (likely(s->assets.c > 0)) {
		for (uint32_t i = 0; i < s->assets.c; i++)
			if (s->assets.v[i].container)
				free(s->assets.v[i].container);

		free(s->assets.v);
		s->assets.c = 0;
		s->assets.v = nullptr;
	}

	if (s->scripts.c > 0) {
		free(s->scripts.v);
		s->scripts.c = 0;
		s->scripts.v = nullptr;
	}

	if (s->externals.c > 0) {
		free(s->externals.v);
		s->externals.c = 0;
		s->externals.v = nullptr;
	}

	assetbundle_free(s->catalog);
	free(s);
}

void *serialized_repack(struct serialized *s)
{
	(void)s;
	/*
	const size_t header_size = 20;
	size_t data_size = 0;
	size_t metadata_size = 0;
	off_t data_start = 0;
	uint32_t buffer_size;
	void *data = nullptr;
	off_t offset = 0;

	writer w;

	metadata_size += strlen(s->u_version);
	metadata_size += align(metadata_size, 4);
	metadata_size += sizeof(s->target_platform);
	metadata_size += sizeof(s->enable_type_tree);
	metadata_size += sizeof(s->types.c);
	for (uint32_t i = 0; i < s->types.c; i++) {
	    if (likely(s->types.v[i].script_type_index >= 0))
	        metadata_size += 47 + 24 * s->types.v[i].nodes.c + s->types.v[i].string_buffer_size;
	    else
	        metadata_size += 31 + 24 * s->types.v[i].nodes.c + s->types.v[i].string_buffer_size;
	}

	metadata_size += sizeof(s->assets.c);
	metadata_size += align(metadata_size, 4);
	metadata_size += 20 * s->assets.c;
	metadata_size += align(metadata_size, 4);

	metadata_size += sizeof(s->scripts.c);
	for (uint32_t i = 0; i < s->scripts.c; i++) {
	    // @TODO write scripts bodies
	}

	metadata_size += sizeof(s->externals.c);
	for (uint32_t i = 0; i < s->externals.c; i++) {
	    metadata_size += 1; // empty
	    metadata_size += 16; // guid
	    metadata_size += 4; // type
	    metadata_size += strlen(s->externals.v[i].path) + 1; // path
	}

	if (likely(s->user_string == nullptr))
	    metadata_size += 1;
	else
	    metadata_size += strlen(s->user_string) + 1;

	for (uint32_t i = 0; i < s->assets.c; i++) {
	    data_size += s->assets.v[i].size;
	    if (likely(i != s->assets.c - 1))
	        data_size += align(data_size, 8);
	}

	if  (unlikely(header_size + metadata_size < 4096)) {
	    buffer_size = data_start = 4096;
	    buffer_size += data_size;
	} else {
	    buffer_size = header_size + metadata_size;
	    buffer_size += align(buffer_size, 16);
	    buffer_size += data_size;

	    data_start = header_size + metadata_size;
	    data_start += align(data_start, 16);
	}

	data = malloc(buffer_size);
	if (unlikely(data == nullptr)) {
	    perror("Error: can not allocate memory to pack serialized file");
	    return nullptr;
	}

	memset(data, 0, buffer_size);

	write_int32be(&w, 0);
	write_int32be(&w, buffer_size);
	write_int32be(&w, s->version);
	write_int32be(&w, data_start);
	write_int32be(&w, s->flags);
	write_string(&w, s->u_version);
	writer_align(&w, 4);
	write_int32le(&w, s->target_platform);
	write_byte(&w, s->enable_type_tree);
	write_int32le(&w, s->types.c);

	for (uint32_t t = 0; t < s->types.c; t++) {
	    write_int32le(&w, s->types.v[t].class_id);
	    write_byte(&w, s->types.v[t].is_stripped);
	    write_int16le(&w, s->types.v[t].script_type_index);
	    write_bytes(&w, s->types.v[t].type_hash, sizeof(s->types.v[t].type_hash));
	    if (likely(s->types.v[t].script_type_index >= 0))
	        write_bytes(&w, s->types.v[t].script_hash, sizeof(s->types.v[t].script_hash));
	    write_int32le(&w, s->types.v[t].nodes.c);
	    write_int32le(&w, s->types.v[t].string_buffer_size);

	    for (uint32_t n = 0; n < s->types.v[t].nodes.c; n++) {
	        write_int16le(&w, s->types.v[t].nodes.v[n].version);
	        write_byte(&w, s->types.v[t].nodes.v[n].level);
	        write_byte(&w, s->types.v[t].nodes.v[n].flags);
	        write_int32le(&w, s->types.v[t].nodes.v[n].type_offset);
	        write_int32le(&w, s->types.v[t].nodes.v[n].name_offset);
	        write_int32le(&w, s->types.v[t].nodes.v[n].byte_size);
	        write_int32le(&w, s->types.v[t].nodes.v[n].index);
	        write_int32le(&w, s->types.v[t].nodes.v[n].metaflags);
	    }

	    if (likely(s->types.v[t].string_buffer_size > 0)) {
	        memcpy(data + offset, s->types.v[t].string_buffer, s->types.v[t].string_buffer_size);
	        offset += s->types.v[t].string_buffer_size;
	    }
	}

	write_int32le(&w, s->assets.c);
	writer_align(&w, 4);

	void *data_offset = 0;

	for (uint32_t e = 0; e < s->assets.c; e++) {
	    write_int64le(&w, s->assets.v[e].path_id);
	    write_int32le(&w, data_offset);
	    write_int32le(&w, s->assets.v[e].size);
	    write_int32le(&w, s->assets.v[e].type->type_id);
	    data_offset += s->assets.v[e].size;
	    if (likely(e != s->assets.c - 1))
	        data_offset += align(data_offset, 8);
	}

	writer_align(&w, 4);

	write_int32le(&w, s->scripts.c);
	if (unlikely(s->scripts.c > 0)) {
	    // @TODO write scripts bodies
	}

	write_int32le(&w, s->externals.c);
	if (unlikely(s->externals.c > 0)) {
	    for (uint32_t e = 0; e < s->externals.c; e++) {
	        write_byte(&w, 0);
	        memcpy((char*)data + offset, s->externals.v[e].guid, sizeof(s->externals.v[e].guid));
	        offset += sizeof(s->externals.v[e].guid);
	        write_int32le(&w, s->externals.v[e].type);
	        write_string(&w, s->externals.v[e].path);
	    }
	    writer_align(&w, 8);
	}

	offset = data_start + data_offset;

	return data;
	*/
	return nullptr;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
