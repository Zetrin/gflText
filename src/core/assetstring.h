#ifndef ASSETNAME_H
#define ASSETNAME_H

#include <stdint.h>

#define ASSET_AABB                         0
#define ASSET_ANIMATION_CLIP               5
#define ASSET_ANIMATION_CURVE              19
#define ASSET_ANIMATION_STATE              34
#define ASSET_ARRAY                        49
#define ASSET_BASE                         55
#define ASSET_BITFIELD                     60
#define ASSET_BITSET                       69
#define ASSET_BOOL                         76
#define ASSET_CHAR                         81
#define ASSET_COLOR_RGBA                   86
#define ASSET_COMPONENT                    96
#define ASSET_DATA                         106
#define ASSET_DEQUE                        111
#define ASSET_DOUBLE                       117
#define ASSET_DYNARRAY                     124
#define ASSET_FAST_PROPERTY                138
#define ASSET_FIRST                        155
#define ASSET_FLOAT                        161
#define ASSET_FONT                         167
#define ASSET_GAMEOBJECT                   172
#define ASSET_GENERIC_MONO                 183
#define ASSET_GRADIENTN                    196
#define ASSET_GUID                         208
#define ASSET_GUI_STYLE                    213
#define ASSET_INT                          222
#define ASSET_LIST                         226
#define ASSET_LONG_LONG                    231
#define ASSET_MAP                          241
#define ASSET_MATRIX_4X4F                  245
#define ASSET_MDFOUR                       256
#define ASSET_MONOBEHAVIOUR                263
#define ASSET_MONO_SCRIPT                  277
#define ASSET_M_BYTESIZE                   288
#define ASSET_M_CURVE                      299
#define ASSET_M_EDITOR_CLASSID             307
#define ASSET_M_EDITOR_FLAGS_HIDE          331
#define ASSET_M_ENABLED                    349
#define ASSET_M_EXTENSION_PTR              359
#define ASSET_M_GAMEOBJECT                 374
#define ASSET_M_INDEX                      387
#define ASSET_M_IS_ARRAY                   395
#define ASSET_M_IS_STATIC                  405
#define ASSET_M_METAFLAG                   416
#define ASSET_M_NAME                       427
#define ASSET_M_OBJECT_FLAGS_HIDE          434
#define ASSET_M_PREFAB_INTERNAL            452
#define ASSET_M_PREFAB_PARENT_OBJ          469
#define ASSET_M_SCRIPT                     490
#define ASSET_M_STATIC_EDITOR_FLAGS        499
#define ASSET_M_TYPE                       519
#define ASSET_M_VERSION                    526
#define ASSET_OBJECT                       536
#define ASSET_PAIR                         543
#define ASSET_PPTR_COMPONENT               548
#define ASSET_PPTR_GAMEOBJEJCT             564
#define ASSET_PPTR_MATERIAL                581
#define ASSET_PPTR_MONOBEHAVIOUR           596
#define ASSET_PPTR_MONOSCRIPT              616
#define ASSET_PPTR_OBJECT                  633
#define ASSET_PPTR_PREFAB                  646
#define ASSET_PPTR_SPRITE                  659
#define ASSET_PPTR_TEXTASSET               672
#define ASSET_PPTR_TEXTURE                 688
#define ASSET_PPTR_TEXTURE2D               702
#define ASSET_PPTR_TRANSFORM               718
#define ASSET_PREFAB                       734
#define ASSET_QUATERNIONF                  741
#define ASSET_RECTF                        753
#define ASSET_RECTINT                      759
#define ASSET_RECTOFFSET                   767
#define ASSET_SECOND                       778
#define ASSET_SET                          785
#define ASSET_SHORT                        789
#define ASSET_SIZE                         795
#define ASSET_SINT16                       800
#define ASSET_SINT32                       807
#define ASSET_SINT64                       814
#define ASSET_SINT8                        821
#define ASSET_STATICVECTOR                 827
#define ASSET_STRING                       840
#define ASSET_TEXTASSET                    847
#define ASSET_TEXTMESH                     857
#define ASSET_TEXTURE                      866
#define ASSET_TEXTURE2D                    874
#define ASSET_TRANSFORM                    884
#define ASSET_TYPELESS                     894
#define ASSET_UINT16                       907
#define ASSET_UINT32                       914
#define ASSET_UINT64                       921
#define ASSET_UINT8                        928
#define ASSET_UINT                         934
#define ASSET_ULONGLONG                    947
#define ASSET_USHORT                       966
#define ASSET_VECTOR                       981
#define ASSET_VECTOR2F                     988
#define ASSET_VECTOR3F                     997
#define ASSET_VECTOR4F                     1006
#define ASSET_M_SCRIPTING_CLASSID          1015
#define ASSET_GRADIENT                     1042
#define ASSET_TYPEPTR                      1051
#define ASSET_INT2_STORAGE                 1057
#define ASSET_INT3_STORAGE                 1070
#define ASSET_BOUNDSINT                    1083
#define ASSET_M_CORRESPONDING_SOURCE_OBECT 1093
#define ASSET_M_PREFAB_INSTANCE            1121
#define ASSET_M_PREFAB_ASSET               1138
#define ASSET_FILESIZE                     1152
#define ASSET_HASH128                      1161

[[maybe_unused]]
static const char *assetname(uint32_t id)
{
	switch (id) {
		case ASSET_AABB:
			return "AABB";
		case ASSET_ANIMATION_CLIP:
			return "AnimationClip";
		case ASSET_ANIMATION_CURVE:
			return "AnimationCurve";
		case ASSET_ANIMATION_STATE:
			return "AnimationState";
		case ASSET_ARRAY:
			return "Array";
		case ASSET_BASE:
			return "Base";
		case ASSET_BITFIELD:
			return "BitField";
		case ASSET_BITSET:
			return "bitset";
		case ASSET_BOOL:
			return "bool";
		case ASSET_CHAR:
			return "char";
		case ASSET_COLOR_RGBA:
			return "ColorRGBA";
		case ASSET_COMPONENT:
			return "Component";
		case ASSET_DATA:
			return "data";
		case ASSET_DEQUE:
			return "deque";
		case ASSET_DOUBLE:
			return "double";
		case ASSET_DYNARRAY:
			return "dynamic_array";
		case ASSET_FAST_PROPERTY:
			return "FastPropertyName";
		case ASSET_FIRST:
			return "first";
		case ASSET_FLOAT:
			return "float";
		case ASSET_FONT:
			return "Font";
		case ASSET_GAMEOBJECT:
			return "GameObject";
		case ASSET_GENERIC_MONO:
			return "Generic Mono";
		case ASSET_GRADIENTN:
			return "GradientNEW";
		case ASSET_GUID:
			return "GUID";
		case ASSET_GUI_STYLE:
			return "GUIStyle";
		case ASSET_INT:
			return "int";
		case ASSET_LIST:
			return "list";
		case ASSET_LONG_LONG:
			return "long long";
		case ASSET_MAP:
			return "map";
		case ASSET_MATRIX_4X4F:
			return "Matrix4x4f";
		case ASSET_MDFOUR:
			return "MdFour";
		case ASSET_MONOBEHAVIOUR:
			return "MonoBehaviour";
		case ASSET_MONO_SCRIPT:
			return "MonoScript";
		case ASSET_M_BYTESIZE:
			return "m_ByteSize";
		case ASSET_M_CURVE:
			return "m_Curve";
		case ASSET_M_EDITOR_CLASSID:
			return "m_EditorClassIdentifier";
		case ASSET_M_EDITOR_FLAGS_HIDE:
			return "m_EditorHideFlags";
		case ASSET_M_ENABLED:
			return "m_Enabled";
		case ASSET_M_EXTENSION_PTR:
			return "m_ExtensionPtr";
		case ASSET_M_GAMEOBJECT:
			return "m_GameObject";
		case ASSET_M_INDEX:
			return "m_Index";
		case ASSET_M_IS_ARRAY:
			return "m_IsArray";
		case ASSET_M_IS_STATIC:
			return "m_IsStatic";
		case ASSET_M_METAFLAG:
			return "m_MetaFlag";
		case ASSET_M_NAME:
			return "m_Name";
		case ASSET_M_OBJECT_FLAGS_HIDE:
			return "m_ObjectHideFlags";
		case ASSET_M_PREFAB_INTERNAL:
			return "m_PrefabInternal";
		case ASSET_M_PREFAB_PARENT_OBJ:
			return "m_PrefabParentObject";
		case ASSET_M_SCRIPT:
			return "m_Script";
		case ASSET_M_STATIC_EDITOR_FLAGS:
			return "m_StaticEditorFlags";
		case ASSET_M_TYPE:
			return "m_Type";
		case ASSET_M_VERSION:
			return "m_Version";
		case ASSET_OBJECT:
			return "Object";
		case ASSET_PAIR:
			return "pair";
		case ASSET_PPTR_COMPONENT:
			return "PPtr<Component>";
		case ASSET_PPTR_GAMEOBJEJCT:
			return "PPtr<GameObject>";
		case ASSET_PPTR_MATERIAL:
			return "PPtr<Material>";
		case ASSET_PPTR_MONOBEHAVIOUR:
			return "PPtr<MonoBehaviour>";
		case ASSET_PPTR_MONOSCRIPT:
			return "PPtr<MonoScript>";
		case ASSET_PPTR_OBJECT:
			return "PPtr<Object>";
		case ASSET_PPTR_PREFAB:
			return "PPtr<Prefab>";
		case ASSET_PPTR_SPRITE:
			return "PPtr<Sprite>";
		case ASSET_PPTR_TEXTASSET:
			return "PPtr<TextAsset>";
		case ASSET_PPTR_TEXTURE:
			return "PPtr<Texture>";
		case ASSET_PPTR_TEXTURE2D:
			return "PPtr<Texture2D>";
		case ASSET_PPTR_TRANSFORM:
			return "PPtr<Transform>";
		case ASSET_PREFAB:
			return "Prefab";
		case ASSET_QUATERNIONF:
			return "Quaternionf";
		case ASSET_RECTF:
			return "Rectf";
		case ASSET_RECTINT:
			return "RectInt";
		case ASSET_RECTOFFSET:
			return "RectOffset";
		case ASSET_SECOND:
			return "second";
		case ASSET_SET:
			return "set";
		case ASSET_SHORT:
			return "short";
		case ASSET_SIZE:
			return "size";
		case ASSET_SINT16:
			return "SInt16";
		case ASSET_SINT32:
			return "SInt32";
		case ASSET_SINT64:
			return "SInt64";
		case ASSET_SINT8:
			return "SInt8";
		case ASSET_STATICVECTOR:
			return "staticvector";
		case ASSET_STRING:
			return "string";
		case ASSET_TEXTASSET:
			return "TextAsset";
		case ASSET_TEXTMESH:
			return "TextMesh";
		case ASSET_TEXTURE:
			return "Texture";
		case ASSET_TEXTURE2D:
			return "Texture2D";
		case ASSET_TRANSFORM:
			return "Transform";
		case ASSET_TYPELESS:
			return "TypelessData";
		case ASSET_UINT16:
			return "UInt16";
		case ASSET_UINT32:
			return "UInt32";
		case ASSET_UINT64:
			return "UInt64";
		case ASSET_UINT8:
			return "UInt8";
		case ASSET_UINT:
			return "unsigned int";
		case ASSET_ULONGLONG:
			return "unsigned long long";
		case ASSET_USHORT:
			return "unsigned short";
		case ASSET_VECTOR:
			return "vector";
		case ASSET_VECTOR2F:
			return "Vector2f";
		case ASSET_VECTOR3F:
			return "Vector3f";
		case ASSET_VECTOR4F:
			return "Vector4f";
		case ASSET_M_SCRIPTING_CLASSID:
			return "m_ScriptingClassIdentifier";
		case ASSET_GRADIENT:
			return "Gradient";
		case ASSET_TYPEPTR:
			return "Type*";
		case ASSET_INT2_STORAGE:
			return "int2_storage";
		case ASSET_INT3_STORAGE:
			return "int3_storage";
		case ASSET_BOUNDSINT:
			return "BoundsInt";
		case ASSET_M_CORRESPONDING_SOURCE_OBECT:
			return "m_CorrespondingSourceObject";
		case ASSET_M_PREFAB_INSTANCE:
			return "m_PrefabInstance";
		case ASSET_M_PREFAB_ASSET:
			return "m_PrefabAsset";
		case ASSET_FILESIZE:
			return "FileSize";
		case ASSET_HASH128:
			return "Hash128";
		default:
			return nullptr;
	}
}

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
