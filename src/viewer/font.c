#include "font.h"

#include <ft2build.h>
#include FT_FREETYPE_H

bool create_font_face(void *font_data, size_t font_data_size)
{
	FT_Library library;
	FT_Error error;
	FT_Face face;

	error = FT_Init_FreeType(&library);
	if (error) {
		// @TODO
	}

	error = FT_New_Memory_Face(library, font_data, font_data_size, 0, &face);
	if (error) {
		// @TODO
	}

	FT_Set_Pixel_Sizes(face, 0, 48);

	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return false;
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
