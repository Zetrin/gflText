#include <GL/gl.h>
#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#include <GLFW/glfw3.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

/*
 * https://learnopengl.com/Advanced-OpenGL/Framebuffers
 */

const GLchar verticies_shader_src[] =
    "#version 300 es \n"
    "layout(location=0) in vec4 position; \n"
    "layout(location=1) in vec3 texture_coordinates; \n"
    "out vec2 interpolated_texture_coordinates; \n"
    "void main() \n"
    "{ \n"
    "    gl_Position = position; \n"
    "    interpolated_texture_coordinates = texture_coordinates.xy; \n"
    "} \n";

const GLchar fragment_shader_src[] =
    "#version 300 es \n"
    "precision highp float; \n"
    "in vec2 interpolated_texture_coordinates; \n"
    "uniform sampler2D texture_sampler; \n"
    "layout(location=0) out vec4 frag_color; \n"
    "void main() \n"
    "{ \n"
    "    frag_color = texture2D(texture_sampler, interpolated_texture_coordinates); \n"
    "} \n";

const GLfloat box_vertices[] = {
    -1.0f,
    -1.0f,
    0.0f,
    1.0f,
    -1.0f,
    0.0f,
    -1.0f,
    1.0f,
    0.0f,
    1.0f,
    1.0f,
    0.0f,
};

const GLfloat texture_vertices[] = {
    0.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    1.0f, 1.0f, 0.0f
};

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	(void)mods;
	(void)scancode;
	switch (key) {
		case GLFW_KEY_Q:
		case GLFW_KEY_ESCAPE:
			if (action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);
			break;
		default:
			break;
	}
}

GLint CompileProgram(GLuint gl_program)
{
	GLint linked, valid;

	glLinkProgram(gl_program);
	glGetProgramiv(gl_program, GL_LINK_STATUS, &linked);

	if (!linked) {
		GLint info_len = 0;

		glGetProgramiv(gl_program, GL_INFO_LOG_LENGTH, &info_len);

		if (info_len) {
			char *infoLog = malloc(sizeof(char) * info_len);

			glGetProgramInfoLog(gl_program, info_len, NULL, infoLog);
			fprintf(stderr, "Error linking program:\n%s\n", infoLog);

			free(infoLog);
		}

		glDeleteProgram(gl_program);

		return -1;
	}

	glValidateProgram(gl_program);
	glGetProgramiv(gl_program, GL_VALIDATE_STATUS, &valid);

	if (!valid) {
		GLint info_len = 0;

		glGetProgramiv(gl_program, GL_INFO_LOG_LENGTH, &info_len);

		if (info_len) {
			char *infoLog = malloc(sizeof(char) * info_len);

			glGetProgramInfoLog(gl_program, info_len, NULL, infoLog);
			fprintf(stderr, "Validation failed:\n%s\n", infoLog);

			free(infoLog);
		}

		glDeleteProgram(gl_program);

		return -1;
	}

	return 0;
}

GLint LoadShader(GLenum type, const char *shaderSrc)
{
	GLuint shader;
	GLint compiled;

	/* Create the shader object */
	shader = glCreateShader(type);

	if (shader == 0)
		return -1;

	/* Load the shader source */
	glShaderSource(shader, 1, &shaderSrc, NULL);

	/* Compile the shader */
	glCompileShader(shader);

	/* Check the compile status */
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled) {
		GLint infoLen = 0;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1) {
			char *infoLog = malloc(sizeof(char) * infoLen);

			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			fprintf(stderr, "Shader compilation failed:\n%s\n", infoLog);

			free(infoLog);
		}

		glDeleteShader(shader);
		return -1;
	}

	return shader;
}

void gl_print_error(const char *func)
{
	GLenum error = glGetError();
	const char *format = "%s: %s\n";
	char *errstr = NULL;

	switch (error) {
		case GL_NO_ERROR:
			return;
		case GL_INVALID_ENUM:
			errstr = "GL_INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			errstr = "GL_INVALID_VALUE";
			break;
		case GL_INVALID_OPERATION:
			errstr = "GL_INVALID_OPERATION";
			break;
		case GL_STACK_OVERFLOW:
			errstr = "GL_STACK_OVERFLOW";
			break;
		case GL_STACK_UNDERFLOW:
			errstr = "GL_STACK_UNDERFLOW";
			break;
		case GL_OUT_OF_MEMORY:
			errstr = "GL_OUT_OF_MEMORY";
			break;
		case GL_TABLE_TOO_LARGE:
			errstr = "GL_TABLE_TOO_LARGE";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			errstr = "GL_INVALID_FRAMEBUFFER_OPERATION";
			break;
		default:
			errstr = "Unknown error code";
			break;
	}

	printf(format, func, errstr);
}

int main(int argc, char *argv[])
{
	int in;
	struct stat st;
	void *addr;

	unsigned int saiga;

	GLFWwindow *window;

	GLuint vertex_shader, fragment_shader;
	GLuint gl_program;

	(void)argc;
	(void)argv;

	in = open("saiga.bin", O_RDONLY | O_NOFOLLOW);
	if (in == -1) {
		perror("Could not open texture file");
		return EXIT_FAILURE;
	}

	if (fstat(in, &st) == -1) {
		perror("Can not stat file");
		close(in);
		return EXIT_FAILURE;
	}

	addr = mmap(nullptr, st.st_size, PROT_READ, MAP_PRIVATE, in, 0);
	if (addr == MAP_FAILED) {
		perror("Can not mmap file");
		close(in);
		return EXIT_FAILURE;
	}
	close(in);

	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

	if (glfwInit() == GLFW_FALSE) {
		munmap(addr, st.st_size);
		return EXIT_FAILURE;
	}

	window = glfwCreateWindow(512, 512, "Girls' Frontline Scene Viewer", NULL, NULL);
	if (window == NULL) {
		munmap(addr, st.st_size);
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, box_vertices);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, texture_vertices);

	vertex_shader = LoadShader(GL_VERTEX_SHADER, verticies_shader_src);
	fragment_shader = LoadShader(GL_FRAGMENT_SHADER, fragment_shader_src);

	glReleaseShaderCompiler();

	gl_program = glCreateProgram();

	glAttachShader(gl_program, vertex_shader);
	glAttachShader(gl_program, fragment_shader);
	CompileProgram(gl_program);

	glUseProgram(gl_program);

	GLuint sampler_location = glGetUniformLocation(gl_program, "texture_sampler");
	gl_print_error("glGetUniformLocation");

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	gl_print_error("glPixelStorei");
	glGenTextures(1, &saiga);
	gl_print_error("glGenTextures");
	glBindTexture(GL_TEXTURE_2D, saiga);
	gl_print_error("glBindTexture");
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);

	glCompressedTexImage2D(GL_TEXTURE_2D, 0, GL_COMPRESSED_RGB8_ETC2, 512, 512, 0, st.st_size, addr);
	gl_print_error("glCompressedTexImage2D");

	glActiveTexture(GL_TEXTURE0);
	gl_print_error("glActiveTexture");

	glBindTexture(GL_TEXTURE_2D, saiga);
	gl_print_error("glBindTexture");

	glUniform1i(sampler_location, 0);
	gl_print_error("glUniform1i");

	glViewport(0, 0, 512, 512);
	gl_print_error("glViewport");

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		gl_print_error("glDrawArrays");
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &saiga);

	munmap(addr, st.st_size);

	glDeleteProgram(gl_program);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
