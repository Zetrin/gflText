#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <threads.h>
#include <unistd.h>

/*
   resourceavgpicprefabs
   assets/resources/dabao/avgpicprefabs/%s.prefab
*/

bool should_stop = false;

void sigint_handler(int signal)
{
	if (signal == SIGINT)
		should_stop = true;
}

static int filter(const struct dirent *entry)
{
	if (entry->d_type != DT_REG)
		return 0;

	return strstr(entry->d_name, ".ab") != nullptr;
}

/*int init(void) {
    struct dirent **abs;
    int amount;
    int ab;
    char *buffer = nullptr;
    struct stat info;
    struct unityfs uf = {0};
    serialized *s = nullptr;
    void *serialized_data = nullptr;
    thrd_t threads[4];

    amount = scandir("New", &abs, filter, alphasort);
    if (amount == -1) {
        perror("Cannot open directory NEW");
        return -1;
    }

    chdir("New");

    buffer = malloc(100*1024*1024);
    if (buffer == nullptr) {
        perror("Cannot allocate memory for buffer");
        goto clean;
    }

    for (int i = 0; i < amount; i++) {
        if (should_stop) {
            free(buffer);
            goto clean;
        }

        printf("%u of %u\n", i, amount);

        ab = open(abs[i]->d_name, O_RDONLY | O_NOFOLLOW);
        if (ab == -1) {
            perror("Could not open file");
            break;
        }
        fstat(ab, &info);
        read(ab, buffer, info.st_size);
        close(ab);

        serialized_data = read_bundle_file(buffer, &uf);
        if (serialized_data != nullptr) {
            s = serialized_file(serialized_data);
            if (s != nullptr)
                free_serialized_file(s);
            else
                printf("Failed to open serialized\n");


            free(serialized_data);
            free_bundle(&uf);
        } else
            printf("Failed to open bundle\n");
    }

    free(buffer);

clean:
    for (int i = 0; i < amount; i++)
        free(abs[i]);
    free(abs);

    return 0;
}*/

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_handler = &sigint_handler;
	sigaction(SIGINT, &act, nullptr);

	exit(EXIT_SUCCESS);
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
