#include <fcntl.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <vulkan/vulkan_core.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

#define MAX_DEVICE_COUNT                 8
#define MAX_QUEUE_COUNT                  4
#define MAX_PRESENT_MODE_COUNT           6
#define MAX_SWAPCHAIN_IMAGES             3
#define FRAME_COUNT                      2
#define PRESENT_MODE_MAILBOX_IMAGE_COUNT 3
#define PRESENT_MODE_DEFAULT_IMAGE_COUNT 2

#define WIDTH  512
#define HEIGHT 512

/*
 * https://raw.githubusercontent.com/sopyer/Vulkan/562e653fbbd1f7a83ec050676b744dd082b2ebed/main.c
 * https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Rendering_and_presentation
 * https://habr.com/ru/articles/587874/
 */

static inline uint32_t clamp_u32(uint32_t value, uint32_t min, uint32_t max)
{
	return value < min ? min : (value > max ? max : value);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	(void)mods;
	(void)scancode;
	switch (key) {
		case GLFW_KEY_Q:
		case GLFW_KEY_ESCAPE:
			if (action == GLFW_PRESS)
				glfwSetWindowShouldClose(window, GL_TRUE);
			break;
		default:
			break;
	}
}

int main(int argc, char *argv[])
{
	GLFWwindow *window;
	VkApplicationInfo appInfo = {
	    .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	    .pApplicationName = "SceneViewer",
	    .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
	    .pEngineName = nullptr,
	    .engineVersion = VK_MAKE_VERSION(1, 0, 0),
	    .apiVersion = VK_API_VERSION_1_3,
	    .pNext = nullptr,
	};
	VkInstanceCreateInfo createInfo = {
	    .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	    .flags = 0,
	    .pApplicationInfo = &appInfo,
	    .enabledLayerCount = 0,
	    .ppEnabledLayerNames = nullptr,
	    .enabledExtensionCount = 0,
	    .ppEnabledExtensionNames = nullptr,
	    .pNext = nullptr,
	};
	VkInstance instance = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkDevice device = VK_NULL_HANDLE;
	VkQueue queue = VK_NULL_HANDLE;
	uint32_t queueFamilyIndex;
	VkSwapchainKHR swapchain = VK_NULL_HANDLE;
	VkCommandPool commandPool = VK_NULL_HANDLE;
	VkCommandBuffer commandBuffers[FRAME_COUNT] = {VK_NULL_HANDLE};
	VkFence frameFences[FRAME_COUNT] = {VK_NULL_HANDLE};
	VkSemaphore imageAvailableSemaphores[FRAME_COUNT] = {VK_NULL_HANDLE};
	VkSemaphore renderFinishedSemaphores[FRAME_COUNT] = {VK_NULL_HANDLE};

	if (glfwInit() == GLFW_FALSE)
		return EXIT_FAILURE;

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	window = glfwCreateWindow(WIDTH, HEIGHT, "Girls' Frontline Scene Viewer", nullptr, nullptr);
	if (window == nullptr) {
		glfwTerminate();
		return EXIT_FAILURE;
	}

	{
		uint32_t glfwExtensionCount;
		const char **glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
		createInfo.enabledExtensionCount = glfwExtensionCount;
		createInfo.ppEnabledExtensionNames = glfwExtensions;
	}

	if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
		glfwTerminate();
		return EXIT_FAILURE;
	}

	if (glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS) {
		vkDestroyInstance(instance, nullptr);
		glfwDestroyWindow(window);
		glfwTerminate();
		return EXIT_FAILURE;
	}

	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	{
		uint32_t physicalDeviceCount;
		VkPhysicalDevice deviceHandles[MAX_DEVICE_COUNT];
		VkQueueFamilyProperties queueFamilyProperties[MAX_QUEUE_COUNT];
		VkPhysicalDeviceProperties deviceProperties;
		VkPhysicalDeviceFeatures deviceFeatures;
		VkPhysicalDeviceMemoryProperties deviceMemoryProperties;

		vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, 0);
		if (physicalDeviceCount == 0) {
			vkDestroyInstance(instance, nullptr);
			glfwTerminate();
			return EXIT_FAILURE;
		}
		physicalDeviceCount = physicalDeviceCount > MAX_DEVICE_COUNT ? MAX_DEVICE_COUNT : physicalDeviceCount;
		vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, deviceHandles);

		for (uint32_t i = 0; i < physicalDeviceCount; i++) {
			uint32_t queueFamilyCount = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(deviceHandles[i], &queueFamilyCount, nullptr);
			queueFamilyCount = queueFamilyCount > MAX_QUEUE_COUNT ? MAX_QUEUE_COUNT : queueFamilyCount;
			vkGetPhysicalDeviceQueueFamilyProperties(deviceHandles[i], &queueFamilyCount, queueFamilyProperties);

			vkGetPhysicalDeviceProperties(deviceHandles[i], &deviceProperties);
			vkGetPhysicalDeviceFeatures(deviceHandles[i], &deviceFeatures);
			vkGetPhysicalDeviceMemoryProperties(deviceHandles[i], &deviceMemoryProperties);
			for (uint32_t j = 0; j < queueFamilyCount; j++) {
				VkBool32 supportsPresent = VK_FALSE;

				vkGetPhysicalDeviceSurfaceSupportKHR(deviceHandles[i], j, surface, &supportsPresent);

				if (supportsPresent && (queueFamilyProperties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
					queueFamilyIndex = j;
					physicalDevice = deviceHandles[i];
					break;
				}
			}

			if (physicalDevice)
				break;
		}

		VkDeviceQueueCreateInfo queueCreateInfo = {
		    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
		    .queueFamilyIndex = queueFamilyIndex,
		    .queueCount = 1,
		    .pQueuePriorities = (const float[]){1.0f},
		    .flags = 0,
		    .pNext = nullptr,
		};

		VkDeviceCreateInfo deviceCreateInfo = {
		    .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
		    .queueCreateInfoCount = 1,
		    .pQueueCreateInfos = &queueCreateInfo,
		    .enabledLayerCount = 0,
		    .ppEnabledLayerNames = nullptr,
		    .enabledExtensionCount = 1,
		    .ppEnabledExtensionNames = (const char *const[]){VK_KHR_SWAPCHAIN_EXTENSION_NAME},
		    .pEnabledFeatures = nullptr,
		    .pNext = nullptr
		};

		if (vkCreateDevice(physicalDevice, &deviceCreateInfo, nullptr, &device) != VK_SUCCESS) {
			vkDestroySurfaceKHR(instance, surface, nullptr);
			vkDestroyInstance(instance, nullptr);
			glfwDestroyWindow(window);
			glfwTerminate();
			return EXIT_FAILURE;
		}

		vkGetDeviceQueue(device, queueFamilyIndex, 0, &queue);
	}

	{
		uint32_t swapchainImageCount;
		VkImage swapchainImages[MAX_SWAPCHAIN_IMAGES];
		VkExtent2D swapchainExtent;
		VkSurfaceFormatKHR surfaceFormat;

		uint32_t formatCount = 1;
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, &surfaceFormat);
		surfaceFormat.format = surfaceFormat.format == VK_FORMAT_UNDEFINED ? VK_FORMAT_B8G8R8A8_UNORM : surfaceFormat.format;

		uint32_t presentModeCount = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr);
		VkPresentModeKHR presentModes[MAX_PRESENT_MODE_COUNT];
		presentModeCount = presentModeCount > MAX_PRESENT_MODE_COUNT ? MAX_PRESENT_MODE_COUNT : presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, presentModes);

		VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR; // always supported.
		for (uint32_t i = 0; i < presentModeCount; i++) {
			if (presentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
				presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
				break;
			}
		}

		swapchainImageCount = presentMode == VK_PRESENT_MODE_MAILBOX_KHR ? PRESENT_MODE_MAILBOX_IMAGE_COUNT : PRESENT_MODE_DEFAULT_IMAGE_COUNT;

		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &surfaceCapabilities);

		swapchainExtent = surfaceCapabilities.currentExtent;
		if (swapchainExtent.width == UINT32_MAX) {
			swapchainExtent.width = clamp_u32(WIDTH, surfaceCapabilities.minImageExtent.width, surfaceCapabilities.maxImageExtent.width);
			swapchainExtent.height = clamp_u32(HEIGHT, surfaceCapabilities.minImageExtent.height, surfaceCapabilities.maxImageExtent.height);
		}

		VkSwapchainCreateInfoKHR swapChainCreateInfo = {
		    .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
		    .surface = surface,
		    .minImageCount = swapchainImageCount,
		    .imageFormat = surfaceFormat.format,
		    .imageColorSpace = surfaceFormat.colorSpace,
		    .imageExtent = swapchainExtent,
		    .imageArrayLayers = 1,
		    .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
		    .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
		    .preTransform = surfaceCapabilities.currentTransform,
		    .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		    .presentMode = presentMode,
		    .clipped = VK_TRUE,
		    .pNext = nullptr
		};

		VkResult result = vkCreateSwapchainKHR(device, &swapChainCreateInfo, 0, &swapchain);
		if (result != VK_SUCCESS) {
			vkDestroyDevice(device, 0);
			vkDestroySurfaceKHR(instance, surface, nullptr);
			vkDestroyInstance(instance, nullptr);
			glfwDestroyWindow(window);
			glfwTerminate();
			return EXIT_FAILURE;
		}

		vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, nullptr);
		vkGetSwapchainImagesKHR(device, swapchain, &swapchainImageCount, swapchainImages);
	}

	{
		VkCommandPoolCreateInfo commandPoolCreateInfo = {
		    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
		    .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
		    .queueFamilyIndex = queueFamilyIndex,
		    .pNext = nullptr
		};

		vkCreateCommandPool(device, &commandPoolCreateInfo, 0, &commandPool);

		VkCommandBufferAllocateInfo commandBufferAllocInfo = {
		    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
		    .commandPool = commandPool,
		    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
		    .commandBufferCount = FRAME_COUNT,
		    .pNext = nullptr
		};

		vkAllocateCommandBuffers(device, &commandBufferAllocInfo, commandBuffers);

		VkSemaphoreCreateInfo semaphoreCreateInfo = {
		    VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
		    .flags = 0,
		    .pNext = nullptr
		};

		vkCreateSemaphore(device, &semaphoreCreateInfo, 0, &imageAvailableSemaphores[0]);
		vkCreateSemaphore(device, &semaphoreCreateInfo, 0, &imageAvailableSemaphores[1]);
		vkCreateSemaphore(device, &semaphoreCreateInfo, 0, &renderFinishedSemaphores[0]);
		vkCreateSemaphore(device, &semaphoreCreateInfo, 0, &renderFinishedSemaphores[1]);

		VkFenceCreateInfo fenceCreateInfo = {
		    .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
		    .flags = VK_FENCE_CREATE_SIGNALED_BIT,
		    .pNext = nullptr
		};

		vkCreateFence(device, &fenceCreateInfo, 0, &frameFences[0]);
		vkCreateFence(device, &fenceCreateInfo, 0, &frameFences[1]);
	}

	uint32_t frameIndex = 0;
	VkImage swapchainImages[MAX_SWAPCHAIN_IMAGES];

	while (!glfwWindowShouldClose(window)) {
		uint32_t index = (frameIndex++) % FRAME_COUNT;
		vkWaitForFences(device, 1, &frameFences[index], VK_TRUE, UINT64_MAX);
		vkResetFences(device, 1, &frameFences[index]);

		uint32_t imageIndex;
		vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, imageAvailableSemaphores[index], VK_NULL_HANDLE, &imageIndex);

		VkCommandBufferBeginInfo beginInfo = {
		    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
		    .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
		    .pInheritanceInfo = nullptr,
		    .pNext = nullptr,
		};
		vkBeginCommandBuffer(commandBuffers[index], &beginInfo);

		VkImageSubresourceRange subResourceRange = {
		    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		    .baseMipLevel = 0,
		    .levelCount = VK_REMAINING_MIP_LEVELS,
		    .baseArrayLayer = 0,
		    .layerCount = VK_REMAINING_ARRAY_LAYERS,
		};

		VkImageMemoryBarrier memoryBarrier1 = {
		    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		    .srcAccessMask = 0,
		    .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
		    .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
		    .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		    .srcQueueFamilyIndex = queueFamilyIndex,
		    .dstQueueFamilyIndex = queueFamilyIndex,
		    .image = swapchainImages[imageIndex],
		    .subresourceRange = subResourceRange,
		    .pNext = nullptr
		};

		vkCmdPipelineBarrier(commandBuffers[index], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier1);

		VkClearColorValue clearColor = {
		    .float32 = {
		                1.0f,
		                0.0f,
		                0.0f,
		                1.0f,
		                },
		};

		vkCmdClearColorImage(commandBuffers[index], swapchainImages[imageIndex], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearColor, 1, &subResourceRange);
		// Change layout of image to be optimal for presenting

		VkImageMemoryBarrier memoryBarrier2 = {
		    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
		    .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
		    .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
		    .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		    .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
		    .srcQueueFamilyIndex = queueFamilyIndex,
		    .dstQueueFamilyIndex = queueFamilyIndex,
		    .image = swapchainImages[imageIndex],
		    .subresourceRange = subResourceRange,
		    .pNext = nullptr
		};

		vkCmdPipelineBarrier(commandBuffers[index], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &memoryBarrier2);

		vkEndCommandBuffer(commandBuffers[index]);

		VkSubmitInfo submitInfo = {
		    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
		    .waitSemaphoreCount = 1,
		    .pWaitSemaphores = &imageAvailableSemaphores[index],
		    .pWaitDstStageMask = (VkPipelineStageFlags[]){VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT},
		    .commandBufferCount = 1,
		    .pCommandBuffers = &commandBuffers[index],
		    .signalSemaphoreCount = 1,
		    .pSignalSemaphores = &renderFinishedSemaphores[index],
		    .pNext = nullptr
		};
		vkQueueSubmit(queue, 1, &submitInfo, frameFences[index]);

		VkPresentInfoKHR presentInfo = {
		    .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
		    .waitSemaphoreCount = 1,
		    .pWaitSemaphores = &renderFinishedSemaphores[index],
		    .swapchainCount = 1,
		    .pSwapchains = &swapchain,
		    .pImageIndices = &imageIndex,
		};
		vkQueuePresentKHR(queue, &presentInfo);

		glfwPollEvents();
	}

	vkDeviceWaitIdle(device);
	vkDestroyFence(device, frameFences[0], 0);
	vkDestroyFence(device, frameFences[1], 0);
	vkDestroySemaphore(device, renderFinishedSemaphores[0], 0);
	vkDestroySemaphore(device, renderFinishedSemaphores[1], 0);
	vkDestroySemaphore(device, imageAvailableSemaphores[0], 0);
	vkDestroySemaphore(device, imageAvailableSemaphores[1], 0);
	vkDestroyCommandPool(device, commandPool, 0);

	vkDestroySwapchainKHR(device, swapchain, 0);
	vkDestroyDevice(device, 0);
	vkDestroySurfaceKHR(instance, surface, nullptr);
	vkDestroyInstance(instance, nullptr);
	glfwDestroyWindow(window);
	glfwTerminate();

	exit(EXIT_SUCCESS);
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
