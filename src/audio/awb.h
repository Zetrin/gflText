#ifndef AWB_H
#define AWB_H

#include <inttypes.h>

#include "io.h"

#define AWB_MAGIC 0x41465332U
#define HCA_MAGIC 0x48434100U

typedef struct awb {
	uint8_t version;
	uint8_t offset_size;
	uint16_t waveid_alignment;
	uint32_t subsongs;
	uint16_t offset_alignemnt;
	uint16_t subkey;
} awb_header;

bool read_awb(reader *r);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
