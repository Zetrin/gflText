#include "acb.h"

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "io.h"

static const char *col_storage_str[] = {
    "s8",
    "u8",
    "s16",
    "u16",
    "s32",
    "u32",
    "s64",
    "u64",
    "float",
    "double",
    "string",
    "binary",
    "guid",
    0
};

static const char *col_type_str[] = {
    0,
    "Zero",
    0,
    "Constant",
    0,
    "Perrow",
    0,
    "Constant2",
    0
};

static utf_column *find_column(utf_table *table, const char *name)
{
	utf_column *result = nullptr;

	for (uint16_t i = 0; i < table->num_cols; i++) {
		if (strcmp(table->cols[i].name, name) == 0) {
			result = &table->cols[i];
			break;
		}
	}

	return result;
}

static bool read_utf(void *data, utf_table *table)
{
	reader r;

	reader_set_read_from_memory(&r, data, 8);

	uint32_t magic = read_int32be(&r);
	if (magic != ACB_MAGIC) {
		reader_release(&r);
		return false;
	}

	uint32_t size = read_int32be(&r);
	r.state.size = size;

	table->table_data = data;
	table->version = read_int16be(&r);
	table->rows_offset = read_int16be(&r) + 8u;
	table->strings_offset = read_int32be(&r) + 8u;
	table->data_offset = read_int32be(&r) + 8u;
	table->names_offset = read_int32be(&r);
	table->num_cols = read_int16be(&r);
	table->row_width = read_int16be(&r);
	table->num_rows = read_int32be(&r);

	table->table_name = r.state.data + table->strings_offset;

	table->cols = calloc(table->num_cols, sizeof(*table->cols));
	if (table->cols == nullptr) {
		perror("Could not allocate columns");
		return false;
	}

	uint32_t col_offset = 0;
	for (uint16_t i = 0; i < table->num_cols; i++) {
		uint32_t value_size = 0;
		uint8_t flags = read_byte(&r);
		uint32_t name_offset = read_int32be(&r);
		char *strings = r.state.data + table->strings_offset;

		table->cols[i].data_type = flags & COL_BITMASK_TYPE;
		table->cols[i].column_type = (flags & COL_BITMASK_STORAGE) >> 4;
		table->cols[i].name = nullptr;

		switch (table->cols[i].data_type) {
			case COL_INT8:
			case COL_UINT8:
				value_size = 1u;
				break;
			case COL_INT16:
			case COL_UINT16:
				value_size = 2u;
				break;
			case COL_INT32:
			case COL_UINT32:
			case COL_FLOAT:
			case COL_STRING:
				value_size = 4u;
				break;
			case COL_INT64:
			case COL_UINT64:
			case COL_DOUBLE:
			case COL_BINARY:
				value_size = 8u;
				break;
			case COL_GUID:
				value_size = 22u;
			default:
				free(table->cols);
				return false;
		}

		if (flags & COLUMN_FLAG_NAME) {
			table->cols[i].name = strings + name_offset;
		}

		if (flags & COLUMN_FLAG_DEFAULT) {
			printf("FLAGS:DEFAULT\n");
			table->cols[i].offset = reader_tell(&r);
			reader_skip(&r, value_size);
		}

		if (flags & COLUMN_FLAG_ROW) {
			uint32_t data_offset = table->rows_offset + table->cols[i].offset + col_offset;

			switch (table->cols[i].data_type) {
				case COL_STRING:
					uint64_t str_col_pos = reader_tell(&r);
					reader_seek(&r, data_offset);
					uint32_t string_offset = read_int32be(&r);
					table->cols[i].offset = table->strings_offset + string_offset;
					table->cols[i].length = 0;
					reader_seek(&r, str_col_pos);
					break;
				case COL_BINARY:
					uint64_t bin_col_pos = reader_tell(&r);
					reader_seek(&r, data_offset);
					table->cols[i].offset = table->data_offset + read_int32be(&r);
					table->cols[i].length = read_int32be(&r);
					reader_seek(&r, bin_col_pos);

					if (table->cols[i].length == 0)
						table->cols[i].offset = 0;

					break;
				default:
					table->cols[i].offset = data_offset;
					table->cols[i].length = value_size;
					break;
			}

			col_offset += value_size;
		}

#if DEBUG
		const char *storage_str = col_storage_str[table->cols[i].data_type];
		printf("[%u] NAME='%s' TYPE=%s DATA=0x%X SIZE=%u\n", i, table->cols[i].name, storage_str, table->cols[i].offset, table->cols[i].length);
#endif
	}

	return true;
}

static void utf_free(utf_table *t)
{
	free(t->cols);
}

bool read_acb(reader *r)
{
	utf_table header;
	utf_table cue_name;

	read_utf(r->state.data, &header);

	uint64_t cue_name_table = find_column(&header, "CueNameTable");
	if (cue_name_table == nullptr) {
		utf_free(&header);
		return false;
	}

	read_utf(header.table_data + cue_name_table->offset, &cue_name);

	// CueNamesTable
	// Cue
	// SequenceTable
	// Track
	// TrackEvent
	// Synth
	// Waveform
	//

	utf_free(&cue_name);
	utf_free(&header);

	return true;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
