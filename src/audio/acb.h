#ifndef ACB_H
#define ACB_H

#include <inttypes.h>

#include "io.h"

#define ACB_MAGIC    0x40555446
#define ACB_MAGIC_BE 0x46545540

typedef enum column_flag {
	COLUMN_FLAG_NAME = 0x10,
	COLUMN_FLAG_DEFAULT = 0x20,
	COLUMN_FLAG_ROW = 0x40,
	COLUMN_FLAG_UNDEFINED = 0x80
} column_flag_t;

typedef enum column_type {
	DATA_ZERO = 0x10,
	DATA_CONSTANT = 0x30,
	DATA_PERROW = 0x50,
	DATA_CONSTANT2 = 0x70,
} column_type_t;

typedef enum storage_type {
	COL_INT8 = 0x00,
	COL_UINT8 = 0x01,
	COL_INT16 = 0x02,
	COL_UINT16 = 0x03,
	COL_INT32 = 0x04,
	COL_UINT32 = 0x05,
	COL_INT64 = 0x06,
	COL_UINT64 = 0x07,
	COL_FLOAT = 0x08,
	COL_DOUBLE = 0x09,
	COL_STRING = 0x0A,
	COL_BINARY = 0x0B,
	COL_GUID = 0x0C,
	/* COL_???	= 0x0d  */
	/* COL_???	= 0x0e, */
	/* COL_???	= 0x0f, */
	COL_BITMASK_STORAGE = 0xf0,
	COL_BITMASK_TYPE = 0x0f,
} storage_type_t;

typedef struct utf_row {
	char *name;
	void *data;
	uint8_t data_type;
	uint32_t length;
} utf8_row;

typedef struct utf_column {
	storage_type_t data_type;
	column_type_t column_type;
	char *name;
	uint32_t offset;
	uint32_t length;
} utf_column;

typedef struct utf_table {
	uint16_t version;
	uint16_t rows_offset;
	uint32_t strings_offset;
	uint32_t data_offset;
	uint32_t names_offset;
	uint16_t num_cols;
	uint16_t row_width;
	uint32_t num_rows;
	char *table_name;
	utf_column *cols;
	void *table_data;
} utf_table;

bool read_acb(reader *r);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
