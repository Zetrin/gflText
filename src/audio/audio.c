#define _GNU_SOURCE
#include <stdlib.h>

#include "acb.h"
#include "awb.h"
#include "io.h"

int main(int argc, char *argv[])
{
	reader r;

	reader_set_read_file(&r, "test.acb");

	uint32_t magic = read_int32be(&r);
	switch (magic) {
		case ACB_MAGIC:
			read_acb(&r);
			reader_release(&r);
			return EXIT_SUCCESS;
			break;
		case AWB_MAGIC:
			read_awb(&r);
			reader_release(&r);
			return EXIT_SUCCESS;
			break;
		default:
			reader_release(&r);
			return EXIT_FAILURE;
	}
}

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
