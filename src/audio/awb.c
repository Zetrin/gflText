#include "awb.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "acb.h"
#include "io.h"

bool read_awb(reader *r)
{
	awb_header header = {0};

	read_bytes(r, &header, sizeof(header));

	for (uint32_t ss = 0; ss < header.subsongs; ss++) {
		uint32_t offset = 0x10;
		uint32_t waveid_offset = offset + ss * header.waveid_alignment;

		reader_seek(r, waveid_offset);
		uint16_t waveid = read_int16le(r);

		offset += header.subsongs * header.waveid_alignment;
		offset += ss * header.offset_size;
		reader_seek(r, offset);
		uint32_t subfile_offset = read_int32le(r);
		subfile_offset += align(subfile_offset, header.offset_alignemnt);
		uint32_t subfile_next = read_int32le(r);
		subfile_offset += align(subfile_offset, header.offset_alignemnt);
		uint32_t subfile_size = subfile_next - subfile_offset;

		// @TODO make loop here

		reader_seek(r, subfile_offset);
		uint32_t subsong_type = read_int32be(r);

		if ((subsong_type & 0x7f7f7f7f) != HCA_MAGIC) {
			// @TODO error
			return false;
		}
	}

	return true;
}

/*static void read_header(acb_t *acb) {
    acb->header.version = read_int16be(acb->data, 0);
    acb->header.rows_offset = read_int16be(acb->data, 2);
    acb->header.strings_offset = read_int32be(acb->data, 4);
    acb->header.data_offset = read_int32be(acb->data, 8);
    acb->header.strings_offset = read_int32be(acb->data, 12);
    acb->header.num_cols = read_int16be(acb->data, 16);
    acb->header.num_rows = read_int16be(acb->data, 18);
    acb->header.row_length = read_int32be(acb->data, 20);
    acb->header.name = strdup((char*)acb->data + acb->header.strings_offset);
}

static void read_columns(acb_t *acb) {
    acb_col_t columns[acb->header.num_cols];
    size_t offset = 24;


    for (unsigned int i = 0; i < acb->header.num_cols; i++) {
        uint8_t data_and_type = read_byte(acb->data, offset);
        offset += BYTE;
        unsigned int name_offset = read_int32be(acb->data, offset);
        offset += INT32;
        columns[i].type = data_and_type & COL_BITMASK_TYPE;
        columns[i].data_type = data_and_type & COL_BITMASK_STORAGE;
    }
}

void acb_release(acb_t *acb) {
    if (acb) {
        if (acb->header.name)
            free(acb->header.name);
        free(acb);
        acb = nullptr;
    }
}

acb_t *acb_from_memory(void *data, size_t size) {
    acb_t *acb = nullptr;
    size_t length;


    if (size <= 8)
        return nullptr;

    if (!is_acb(data)) {
        fprintf(stderr, "Not a ACB file\n");
        return nullptr;
    }

    length = read_int32be(data, 4);
    if (length - 8 >= size) {
        fprintf(stderr, "Corrupted file: wrong size\n");
        return nullptr;
    }

    acb = malloc(sizeof(*acb));
    if (acb == nullptr) {
        perror("Could not allocate memory for ACB struct");
        return nullptr;
    }

    acb->data = data + 8;
    acb->data_size = length;

    read_header(acb);
    if (acb->header.version != 0x00 && acb->header.version != 0x01) {
        fprintf(stderr, "Unknows @UTF version\n");
        free(acb->header.name);
        free(acb);
        return nullptr;
    }

    read_columns(acb);

    return acb;
}

acb_t *acb_from_file(const char *filename) {
    acb_t *acb = nullptr;
    struct stat sb;
    void *data = nullptr;
    int fd;
    size_t ret;


    fd = open(filename, O_RDONLY | O_NOFOLLOW);
    if (fd == -1) {
        perror("Could not open ACB file");
        return nullptr;
    }

    fstat(fd, &sb);

    data = malloc(sb.st_size);
    if (data == nullptr) {
        perror("Could not allocate memory");
        goto clean;
    }

    ret = read(fd, data, sb.st_size);
    if ((ssize_t)ret != sb.st_size) {
        perror("Could not read memory");
        goto clean;
    }

    acb = acb_from_memory(data, sb.st_size);

clean:
    close(fd);
    if (data)
        free(data);
    return acb;
}

int main(void) {
    acb_t *acb;

    acb = acb_from_file("build/test.acb");

    acb_release(acb);

    return EXIT_SUCCESS;
}*/

/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
