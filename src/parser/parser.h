#ifndef PARSER_H
#define PARSER_H

#include <stddef.h>
#include <stdint.h>

#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8) + (uint32_t)(((const uint8_t *)(d))[0]))

#define OP_BACKGROUND         "BIN"
#define OP_BACKGROUND_LAYER   "边框"
#define OP_BGM                "BGM"
#define OP_CLEAR_EFFECTS      "关闭蒙版"
#define OP_CLEAR_FIRE         "关闭火花"
#define OP_CREDITS            "名单"
#define OP_EFFECT_EYES        "睁眼"
#define OP_EFFECT_FIRE        "火花"
#define OP_EFFECT_PAN         "平移"
#define OP_EFFECT_SHAKE1      "震屏"
#define OP_EFFECT_SHAKE2      "震屏2"
#define OP_EFFECT_SHAKE3      "震屏3"
#define OP_EFFECT_SNOW        "下雪"
#define OP_EFFECT_NIGHT       "Night"
#define OP_PLAY1              "SE1"
#define OP_PLAY2              "SE2"
#define OP_PORTRAIT_COMMBOX   "通讯框"
#define OP_PORTRAIT_GRAY      "Gray"
#define OP_PORTRAIT_ORANGE    "回忆"
#define OP_PORTRAIT_POSITION  "Position"
#define OP_PORTRAIT_SCALE     "Scale"
#define OP_PORTRAIT_SHAKE     "Shake"
#define OP_SPEAKER            "Speaker"
#define OP_TRANSITION_FADEIN  "黑屏2"
#define OP_TRANSITION_FADEOUT "黑屏1"

#define HASH_TAG_BGM               0xD215099D
#define HASH_TAG_BIN               0xF2797587
#define HASH_TAG_NIGHT             0x90161787
#define HASH_TAG_PORTRAIT_COMMBOX  0x95666C24
#define HASH_TAG_PORTRAIT_POSITION 0x459D0E8A
#define HASH_TAG_PORTRAIT_SCALE    0x7EB14A44
#define HASH_TAG_SE1               0x3CDCF140
#define HASH_TAG_SE2               0x3F88B1D3
#define HASH_TAG_SPEAKER           0x34EA27AC
#define HASH_TAG_边框              0x294AABBE
#define HASH_TAG_黑屏1             0x8ADA4368
#define HASH_TAG_黑屏2             0xCADB757D

enum state {
	STATE_NONE,
	STATE_START,
	STATE_BRACKET_OPEN,
	STATE_BRACKET_IN,
	STATE_BRACKET_CLOSE,
	STATE_TAG_OPEN,
	STATE_TAG_CLOSE,
	STATE_TAG_START_OPEN,
	STATE_TAG_START_INSIDE,
	STATE_TAG_START_CLOSE,
	STATE_TAG_VALUE,
	STATE_TAG_END_OPEN,
	STATE_TAG_END_INSIDE,
	STATE_TAG_END_SLASH,
	STATE_TAG_END_CLOSE,
	STATE_ACTOR,
	STATE_ACTOR_SEPARATOR,
	STATE_SEPARATOR,
	STATE_WANT_COLON,
	STATE_COLON,
	STATE_TEXT,
	STATE_ENDLINE,
	STATE_ERROR,
	STATE_END,
};

enum tag {
	TAG_BACKGROUND,
	TAG_BACKGROUND_LAYER,
	TAG_BGM,
	TAG_FADEIN,
	TAG_FADEOUT,
	TAG_FRAME,
	TAG_NIGHT,
	TAG_NONE,
	TAG_PLAY1,
	TAG_PLAY2,
	TAG_SPEAKER,
	TAG_PORTRAIT_COMMBOX,
	TAG_PORTRAIT_POSITION,
};

struct parser {
	char *text;
	size_t len;
	uint32_t pos;
	unsigned int line;
	unsigned int state;
	unsigned int ctx_state;
};

struct actor {
	char portrait[32];
	struct {
		float x, y;
	} position;
	bool commbox;
	bool gray;
	float shake;
};

typedef enum state state_t;
typedef enum tag tag_t;
typedef struct parser parser_t;

__attribute__((unused)) static int error_line = 0;
__attribute__((unused)) static int error_number = 0;

void init_parser(struct parser *parser, const char *text, size_t length);
int parse_line(struct parser *parser);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
