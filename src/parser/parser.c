#define _GNU_SOURCE
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "parser.h"

void init_parser(struct parser *p, const char *text, size_t length)
{
	p->text = (char *)text;
	p->len = length;
	p->pos = p->line = 0;
}

/* SuperFashHash by Paul Hsieh */
static uint32_t make_hash(const char *data, int len)
{
	uint32_t hash = len, tmp;
	int rem;

	if (len <= 0 || data == nullptr)
		return 0;

	rem = len & 3;
	len >>= 2;

	/* Main loop */
	for (; len > 0; len--) {
		hash += get16bits(data);
		tmp = (get16bits(data + 2) << 11) ^ hash;
		hash = (hash << 16) ^ tmp;
		data += 2 * sizeof(uint16_t);
		hash += hash >> 11;
	}

	/* Handle end cases */
	switch (rem) {
		case 3:
			hash += get16bits(data);
			hash ^= hash << 16;
			hash ^= ((signed char)data[sizeof(uint16_t)]) << 18;
			hash += hash >> 11;
			break;
		case 2:
			hash += get16bits(data);
			hash ^= hash << 11;
			hash += hash >> 17;
			break;
		case 1:
			hash += (signed char)*data;
			hash ^= hash << 10;
			hash += hash >> 1;
			break;
	}

	/* Force "avalanching" of final 127 bits */
	hash ^= hash << 3;
	hash += hash >> 5;
	hash ^= hash << 4;
	hash += hash >> 17;
	hash ^= hash << 25;
	hash += hash >> 6;

	return hash;
}

[[maybe_unused]]
static enum tag get_tag(const char *token)
{
	enum tag tag = TAG_NONE;
	uint32_t hash = 0;
	size_t len;

	len = strlen(token);
	hash = make_hash(token, len);

	switch (hash) {
		case HASH_TAG_SPEAKER:
			return TAG_SPEAKER;
		case HASH_TAG_BGM:
			return TAG_BGM;
		case HASH_TAG_BIN:
			return TAG_BACKGROUND;
		case HASH_TAG_NIGHT:
			return TAG_NIGHT;
		case HASH_TAG_SE1:
			return TAG_PLAY1;
		case HASH_TAG_SE2:
			return TAG_PLAY2;
		case HASH_TAG_边框:
			return TAG_BACKGROUND_LAYER;
		case HASH_TAG_黑屏2:
			return TAG_FADEIN;
		case HASH_TAG_黑屏1:
			return TAG_FADEOUT;
		case HASH_TAG_PORTRAIT_COMMBOX:
			return TAG_PORTRAIT_COMMBOX;
		case HASH_TAG_PORTRAIT_POSITION:
			return TAG_PORTRAIT_POSITION;
		default:
			return tag;
	}
}

__attribute__((unused)) static int parse_actor(const char *str)
{
	return str[1];
}

static int parse_actors(const char *str)
{
	char tk[512] = {0};
	unsigned int t = 0;
	unsigned int n = 0;
	state_t s = STATE_START;

	memset(tk, 0, sizeof(tk));

	while (1) {
		switch (str[n]) {
			case '(':
				switch (s) {
					case STATE_START:
					case STATE_ACTOR:
						s = STATE_BRACKET_OPEN;
						goto write;
					default:
						goto error;
				}
				break;
			case ')':
				switch (s) {
					case STATE_BRACKET_OPEN:
					case STATE_BRACKET_IN:
						s = STATE_BRACKET_CLOSE;
						goto write;
						break;
					default:
						goto error;
				}
				break;
			case '<':
				switch (s) {
					case STATE_ACTOR:
					case STATE_BRACKET_CLOSE:
						goto write;
					default:
						goto error;
				}
				break;
			case '>':
				switch (s) {
					case STATE_ACTOR:
						goto write;
					default:
						goto error;
				}
				break;
			case ';':
				switch (s) {
					case STATE_ACTOR:
					case STATE_BRACKET_CLOSE:
						s = STATE_ACTOR_SEPARATOR;
						printf("ACTOR:%s\n", tk);
						t = 0;
						memset(tk, 0, sizeof(tk));
						break;
					default:
						goto error;
				}
				break;
			case '\0':
				switch (s) {
					case STATE_BRACKET_CLOSE:
					case STATE_ACTOR:
						printf("ACTOR:%s\n", tk);
						goto done;
					default:
						goto error;
				}
				break;
			default:
				switch (s) {
					case STATE_START:
					case STATE_ACTOR_SEPARATOR:
					case STATE_BRACKET_CLOSE:
						s = STATE_ACTOR;
						break;
					case STATE_BRACKET_OPEN:
						switch (str[n]) {
							case '0':
							case '1':
							case '2':
							case '3':
							case '4':
							case '5':
							case '6':
							case '7':
							case '8':
							case '9':
								s = STATE_BRACKET_IN;
								goto write;
							default:
								goto error;
						}
						break;
					case STATE_ACTOR:
						break;
					case STATE_BRACKET_IN:
						goto error;
					default:
						goto error;
				}
			write:
				tk[t++] = str[n];
				break;
		}
		n++;
	}

error:
	return n;

done:
	return 0;
}

int parse_line(struct parser *p)
{
	char tk[512] = {0};
	unsigned int t = 0;
	unsigned int n = 0;
	char *str = p->text;
	state_t s = STATE_START;

	if (p->pos >= p->len)
		return 0;

	memset(tk, 0, sizeof(tk));

	while (1) {
		switch (str[p->pos + n]) {
			case '|':
				switch (s) {
					case STATE_START:
						s = STATE_SEPARATOR;
						printf("ACTORS:%s\n", tk);
						int r = parse_actors(tk);
						if (r > 0) {
							n = r;
							goto error;
						}
						memset(tk, 0, sizeof(tk));
						t = 0;
						break;
					case STATE_SEPARATOR:
						s = STATE_WANT_COLON;
						break;
					case STATE_TEXT:
						tk[t++] = str[p->pos + n];
						break;
					default:
						goto error;
				}
				break;
			case ':':
				switch (s) {
					case STATE_WANT_COLON:
						s = STATE_TEXT;
						/* parse_actors(tk); */
						printf("OPCODES:%s\n", tk);
						memset(tk, 0, sizeof(tk));
						t = 0;

						break;
					case STATE_START:
					case STATE_SEPARATOR:
						goto error;
					default:
						tk[t++] = str[p->pos + n];
						break;
				}
				break;

			case 0xD:
				switch (s) {
					case STATE_TEXT:
						s = STATE_ENDLINE;
						printf("TEXT:%s\n", tk);
						break;
					default:
						goto error;
				}
				break;
			case 0xA:
				switch (s) {
					case STATE_ENDLINE:
						goto done;
					default:
						goto error;
				}
				break;
			case '\0':
				goto stop;
			default:
				switch (s) {
					case STATE_SEPARATOR:
						goto error;
					default:
						tk[t++] = str[p->pos + n];
						break;
				}
				break;
		}

		n++;
		if (n >= p->len) {
			goto error;
		}
	}

done:
	printf("-------------ENDLINE---------------\n");
	p->pos = p->pos + n + 1;
	p->line++;
	return 1;

stop:
	printf("-------------ENDLINE---------------\n");
	return 0;

error:
	printf("Syntax error at line %u position %u\n", p->line + 1, n + 1);
	return -1;
}

int main(int argc, char *argv[])
{
	int file;
	struct stat sb;
	char *text;
	struct parser parser;

	if (argc > 1) {
		file = open(argv[1], O_RDONLY | O_NOFOLLOW);
	} else {
		file = open("text.txt", O_RDONLY | O_NOFOLLOW);
	}

	fstat(file, &sb);
	text = malloc(sb.st_size + 1);
	if (text == nullptr) {
		perror("Could not allocate memory for script");
		close(file);
		return EXIT_FAILURE;
	}
	memset(text, 0, sb.st_size + 1);
	int ret = read(file, text, sb.st_size);
	if (ret != sb.st_size) {
		perror("Could not read file");
		free(text);
		close(file);
		return EXIT_FAILURE;
	}
	close(file);

	init_parser(&parser, text, sb.st_size);

	do {
		ret = parse_line(&parser);
	} while (ret > 0);

	free(text);
	return ret;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
