#ifndef PACKER_H
#define PACKER_H

bool list(const char *filename);
bool export(const char *filename);
bool decrypt(const char *filename);
bool unpack(const char *filename);
bool dump(const char *filename);

#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
