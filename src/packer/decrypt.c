#define _GNU_SOURCE
#include <limits.h>
#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "io/reader.h"
#include "io/writer.h"

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

static const uint8_t key[16] = {
    0x55, 0x6E, 0x69, 0x74, 0x79, 0x46, 0x53, 0x00, 0x00, 0x00, 0x00, 0x07, 0x35, 0x2E, 0x78, 0x2E
};

bool decrypt(const char *filename)
{
	const char *base = basename(filename);
	char out[PATH_MAX];
	uint8_t buf[16], file_key[16];
	uint8_t *rem = nullptr;

	LOG("File %s\n", base);

	struct reader *reader = reader_create_file_reader(filename);
	if (reader == nullptr)
		return false;

	reader_get_bytes(reader, buf, sizeof(buf));
	reader_seek(reader, 0);

	for (uint32_t i = 0; i < sizeof(buf); i++)
		file_key[i] = buf[i] ^ key[i];

	snprintf(out, PATH_MAX, "%s.decrypted", base);

	struct writer *writer = writer_create_file_writer(out);
	if (writer == nullptr) {
		reader_free(reader);
		return false;
	}

	if (setjmp(reader->jmp_buf)) {
		reader_free(reader);
		writer_free(writer);
		if (rem)
			free(rem);
		return false;
	}

	if (setjmp(writer->jmp_buf)) {
		reader_free(reader);
		writer_free(writer);
		if (rem)
			free(rem);
		return false;
	}

	while (reader->offset < 0x8000) {
		const size_t size = MIN(sizeof(buf), 0x8000 - reader->offset);

		reader_get_bytes(reader, buf, size);

		for (uint32_t i = 0; i < size; i++)
			buf[i] = buf[i] ^ file_key[i % sizeof(file_key)];

		writer_put_bytes(writer, buf, size);
	}

	const size_t rem_size = reader->size - reader->offset;
	rem = malloc(rem_size);
	if (rem == nullptr) {
		reader_free(reader);
		writer_free(writer);
		return false;
	}
	reader_get_bytes(reader, rem, rem_size);
	writer_put_bytes(writer, rem, rem_size);

	free(rem);
	reader_free(reader);
	writer_free(writer);

	return true;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
