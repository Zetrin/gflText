#define _GNU_SOURCE
#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "unityfs.h"

bool dump(const char *filename)
{
	unityfs uf;
	const char *base = basename(filename);
	char out[PATH_MAX];
	struct reader *reader;
	struct writer *writer;

	snprintf(out, PATH_MAX, "%s.sbin", base);

	LOG("File %s\n", base);

	reader = reader_create_file_reader(filename);
	if (reader == nullptr)
		return false;

	writer = writer_create_file_writer(out);
	if (writer == nullptr) {
		reader_free(reader);
		return false;
	}

	uf.reader = reader;
	uf.writer = writer;

	if (!bundle_read_header_and_blocks(&uf)) {
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	const bool unpacked = bundle_unpack(&uf);

	bundle_free(&uf);

	reader_free(reader);
	writer_free(writer);

	return unpacked;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
