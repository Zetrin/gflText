#define _GNU_SOURCE
#include <setjmp.h>
#include <stdio.h>
#include <string.h>

#include "assets/assetbundle.h"
#include "common.h"
#include "io/reader.h"
#include "io/writer.h"
#include "serialized.h"
#include "unityfs.h"

static bool list_assets(struct serialized *s)
{
	char title[1024] = {0};

	printf("Reading %s bundle...\n", s->catalog->bundle_name);

	if (setjmp(s->reader->jmp_buf))
		return false;

	assetbundle_link_containers(s->catalog, s->assets);

	for (uint32_t i = 0; i < s->assets.c; i++) {
		const char *class = classid_string(s->assets.v[i].type->class_id);
		const uint32_t offset = s->assets.v[i].data_offset;
		const uint32_t size = s->assets.v[i].size;

		reader_seek(s->reader, s->assets.v[i].data_offset);
		const uint32_t title_length = reader_get_32(s->reader);
		reader_get_bytes(s->reader, title, title_length);
		title[title_length] = 0;

		if (s->assets.v[i].container)
			printf("\t0x%x \"%s\" %s %s SIZE:%u\n", offset, title, class, s->assets.v[i].container, size);
		else
			printf("\t0x%x \"%s\" %s\n", offset, title, class);
	}

	return true;
}

bool list(const char *filename)
{
	unityfs uf = {0};
	const char *base = basename(filename);
	bool ret = false;
	struct reader *reader;
	struct writer *writer;

	LOG("File %s\n", base);

	reader = reader_create_file_reader(filename);
	if (reader == nullptr)
		return false;

	writer = writer_create_memory_writer();
	if (writer == nullptr) {
		reader_free(reader);
		return false;
	}

	uf.reader = reader;
	uf.writer = writer;

	if (!bundle_read_header_and_blocks(&uf)) {
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	if (bundle_unpack(&uf)) {
		struct serialized *s = serialized_from_memory(uf.writer->data, uf.writer->size);
		if (s) {
			ret = list_assets(s);
			reader_free(s->reader);
			serialized_free(s);
		}
	}
	bundle_free(&uf);

	writer_free(writer);
	reader_free(reader);

	return ret;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
