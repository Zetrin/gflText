#define _GNU_SOURCE
#include <errno.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "assets/assetbundle.h"
#include "assets/font.h"
#include "assets/text.h"
#include "io/reader.h"
#include "io/writer.h"
#include "serialized.h"
#include "unityfs.h"
#ifdef TEXTURES
#include "assets/texture2d.h"
#endif
#include "common.h"

extern bool verbose;

static int make_path(const char *const path)
{
	char buf[PATH_MAX];
	const size_t len = strlen(path);
	char *a;

	memcpy(&buf, path, len + 1);

	a = buf;
	while (*a != '\0') {
		if (a == &buf[0] && *a++ == '/')
			continue;

		if (*(++a) != '/')
			continue;

		*a = '\0';

		if (mkdir(buf, S_IRWXU) < 0) {
			if (errno != EEXIST) {
				fprintf(stderr, "Can not create directory %s: %s\n", buf, strerror(errno));
				return -1;
			}
		}

		*a++ = '/';
	}

	return 0;
}

static bool save_to_file(const char *path, const void *data, const size_t size)
{
	struct writer w;

	if (make_path(path) == -1)
		return false;

	if (writer_set_write_file(&w, path)) {
		writer_put_bytes(&w, data, size);
		writer_close(&w);
		return true;
	}

	return false;
}

#ifdef TEXTURES
extern void DecodeETC1(const void *data, int32_t width, int32_t height, void *image);
extern void DecodeASTC(const void *data, int32_t width, int32_t height, int32_t blockWidth, int32_t blockHeight, void *image);
extern void UnpackUnityCrunch(const void *data, uint32_t dataSize, void **ppResult, uint32_t *pResultSize);

static void my_decode_etc1(uint8_t *in, uint8_t *out, size_t width, size_t height)
{
	const uint32_t num_blocks_x = (width + 3) / 4;
	const uint32_t num_blocks_y = (height + 3) / 4;

	for (uint32_t by = 0; by < num_blocks_y; by++) {
		for (uint32_t bx = 0; bx < num_blocks_x; bx++, in += 8u) {
			uint32_t buffer[16] = {0};

			// decode_etc1_block(in, buffer);

			for (unsigned int i = 0, k = 0; i < 4u; i++) {
				for (uint32_t j = 0; j < 12u; j += 3u, k++) {
					uint32_t y = (4 * by + i) * width * 3u;
					uint32_t x = bx * 3u * 4u + j;

					uint8_t colors[4];
					memcpy(colors, &buffer[k], sizeof(buffer[0]));

					*(out + y + x + 0) = (buffer[k] >> 0x0U) & 0xFF;
					*(out + y + x + 1) = (buffer[k] >> 0x8U) & 0xFF;
					*(out + y + x + 2) = (buffer[k] >> 0xFU) & 0xFF;
				}
			}
		}
	}
}

static void my_decode_astc(uint8_t *in, uint8_t *out, const size_t width, const size_t height)
{
	const uint32_t num_blocks_x = (width + 5) / 6;
	const uint32_t num_blocks_y = (height + 5) / 6;
	uint32_t buffer[144];

	for (uint32_t by = 0; by < num_blocks_y; by++) {
		for (uint32_t bx = 0; bx < num_blocks_x; bx++, in += 8u) {
			uint32_t buffer[16] = {0};

			// decode_block(in, buffer);

			for (unsigned int i = 0, k = 0; i < 4u; i++) {
				for (uint32_t j = 0; j < 12u; j += 3u, k++) {
					uint32_t y = (4 * by + i) * width * 3u;
					uint32_t x = bx * 3u * 4u + j;

					uint8_t colors[4];
					memcpy(colors, &buffer[k], sizeof(buffer[0]));

					*(out + y + x + 0) = (buffer[k] >> 0x0U) & 0xFF;
					*(out + y + x + 1) = (buffer[k] >> 0x8U) & 0xFF;
					*(out + y + x + 2) = (buffer[k] >> 0xFU) & 0xFF;
				}
			}
		}
	}
}

static void decode_argb32(uint8_t *in, uint8_t *out, const size_t width, const size_t height)
{
	const uint32_t len = width * height * 4;

	for (uint32_t w = 0; w < len; w += 4) {
		out[w + 0] = in[w + 2];
		out[w + 1] = in[w + 1];
		out[w + 2] = in[w + 0];
		out[w + 3] = in[w + 3];
	}
}

static bool make_png(png_image img, void **png_data, size_t *png_size, void *pixels)
{
	void *png_img_data = nullptr;
	size_t png_img_size = 0;
	bool ret;

	ret = png_image_write_to_memory(&img, nullptr, &png_img_size, false, pixels, 0, nullptr);
	if (!ret) {
		perror("Can not write PNG data");
		free(png_img_data);
		return false;
	}

	png_img_data = malloc(png_img_size);
	if (png_img_data == nullptr) {
		perror("Can not allocate memory for PNG image");
		return false;
	}

	ret = png_image_write_to_memory(&img, png_img_data, &png_img_size, false, pixels, 0, nullptr);
	if (!ret) {
		perror("Can not write PNG data");
		free(png_img_data);
		return false;
	}

	*png_data = png_img_data;
	*png_size = png_img_size;

	return true;
}

[[maybe_unused]]
static void flip(void *data, size_t height, size_t width, unsigned int components)
{
	const unsigned int row_size = width * components;
	void *tmp = malloc(row_size);

	if (tmp) {
		for (size_t h = 0, hb = height - h - 1; hb > h; h++, hb--) {
			void *row = (void *)((uintptr_t)data + (h * row_size));
			void *rowb = (void *)((uintptr_t)data + (hb * row_size));

			memcpy(tmp, row, row_size);
			memcpy(row, rowb, row_size);
			memcpy(rowb, tmp, row_size);
		}
	}

	free(tmp);
}

[[maybe_unused]]
static void drop_rgba_alpha(uint8_t *data, size_t len)
{
	uint32_t i = 4, j = 3;

	for (; i < len; i += 4, j += 3) {
		*(data + j + 0) = *(data + i + 0);
		*(data + j + 1) = *(data + i + 1);
		*(data + j + 2) = *(data + i + 2);
	}
}

static bool texture2d_to_png(const texture2d_asset *t2d, void **png_data, size_t *png_size)
{
	void *pixels = nullptr;
	png_image png_img = {0};
	bool ret = false;

	png_img.height = t2d->height;
	png_img.width = t2d->width;
	png_img.flags = 0;
	png_img.version = PNG_IMAGE_VERSION;

	switch (t2d->format) {
		case TEXTURE_FORMAT_ASTC_RGBA_6x6:
			png_img.format = PNG_FORMAT_RGBA;

			pixels = malloc(t2d->width * t2d->height * 4);
			if (pixels == nullptr) {
				perror("Can not allocate memory for ASTC RGBA6 pixels");
				return false;
			}

			// DecodeASTC(t2d->data, t2d->width, t2d->height, 6, 6, pixels);
			my_decode_astc(t2d->data, pixels, t2d->width, t2d->height);
			break;

		case TEXTURE_FORMAT_ARGB32:
			png_img.format = PNG_FORMAT_BGRA;

			pixels = malloc(t2d->width * t2d->height * 4);
			if (pixels == nullptr) {
				perror("Can not allocate memory for ARGB32 pixels");
				return false;
			}

			decode_argb32(t2d->data, pixels, t2d->width, t2d->height);
			// flip(pixels, t2d->height, t2d->width, 4);
			break;

		case TEXTURE_FORMAT_ETC_RGB4Crunched:
			png_img.format = PNG_FORMAT_BGRA;

			pixels = malloc(t2d->width * t2d->height * 4);
			if (pixels == nullptr) {
				perror("Can not allocate memory for ETC1RGB4CRN pixels");
				return false;
			}

			uint32_t unpacked_size;
			void *unpacked;

			UnpackUnityCrunch(t2d->data, t2d->size, &unpacked, &unpacked_size);
			DecodeETC1(unpacked, t2d->width, t2d->height, pixels);
			free(unpacked);
			// flip(pixels, t2d->height, t2d->width, 4);
			// drop_rgba_alpha(pixels, t2d->width * t2d->height * 4);
			break;

		case TEXTURE_FORMAT_ETC_RGB4:
			png_img.format = PNG_FORMAT_BGRA;

			pixels = malloc(t2d->width * t2d->height * 4);
			if (pixels == nullptr)
				return false;

			DecodeETC1(t2d->data, t2d->width, t2d->height, (uint32_t *)pixels);
			// flip(pixels, t2d->height, t2d->width, 4);
			// drop_rgba_alpha(pixels, t2d->width * t2d->height * 4);
			break;

		case TEXTURE_FORMAT_ALPHA8:
			png_img.format = PNG_FORMAT_GRAY;
			png_img.flags |= PNG_IMAGE_FLAG_COLORSPACE_NOT_sRGB;

			pixels = malloc(t2d->size);
			if (pixels == nullptr)
				return false;

			memcpy(pixels, t2d->data, t2d->size);
			// flip(pixels, t2d->height, t2d->width, 1);
			break;

		default:
			printf("Unsupported texture format\n");
			return false;
	}

	ret = make_png(png_img, png_data, png_size, pixels);
	free(pixels);

	return ret;
}
#endif

struct container {
	uint32_t file_id;
	uint64_t path_id;
	char *name;
	uint32_t lenght;
};

static bool export_data(const struct serialized *s)
{
	char bundle_name[64] = {0};
	char path[PATH_MAX];

	if (s->catalog == nullptr) {
		fprintf(stderr, "Can not export files because bundle has no container info\n");
		return false;
	}

	printf("Export %s bundle...\n", s->catalog->bundle_name);

	assetbundle_link_containers(s->catalog, s->assets);

	memcpy(bundle_name, s->catalog->bundle_name, s->catalog->bundle_name_len);

	for (uint32_t i = 0; i < s->assets.c; i++) {
		if (s->assets.v[i].container == nullptr)
			continue;

		const int len = snprintf(path, sizeof(path), "%s/%s", bundle_name, s->assets.v[i].container);
		path[len] = '\0';

		switch (s->assets.v[i].type->class_id) {
			case CLASSID_TEXTASSET:
				reader_seek(s->reader, s->assets.v[i].data_offset);

				struct text_asset *text = parse_text_asset(s->reader);
				if (text == nullptr)
					continue;

				save_to_file(path, text->body, text->body_length);
				destroy_text_asset(text);
				break;

			case CLASSID_FONT:
				reader_seek(s->reader, s->assets.v[i].data_offset);
				struct font_asset *font = parse_font_asset(s->reader);
				if (font == nullptr)
					continue;

				if (font->data == nullptr) {
					destroy_font_asset(font);
					continue;
				}

				save_to_file(path, font->data, font->data_length);
				destroy_font_asset(font);
				break;

#ifdef TEXTURES
			case CLASSID_TEXTURE2D:
				reader_seek(s->reader, s->assets.v[i].data_offset);
				size_t png_size = 0;
				void *png_data = nullptr;
				texture2d_asset *t2d = parse_texture2d_asset(s->reader);
				if (t2d == nullptr)
					continue;

				if (!texture2d_to_png(t2d, &png_data, &png_size)) {
					destroy_texture2d_asset(t2d);
					continue;
				}

				save_to_file(path, png_data, png_size);
				free(png_data);

				destroy_texture2d_asset(t2d);
				break;
#endif

			default:
				if (verbose)
					DBG("\t%s ignored.\n", classid_string(s->assets.v[i].type->class_id));
				continue;
		}

		printf("\tExport %s (%s)\n", path, classid_string(s->assets.v[i].type->class_id));
	}

	return 0;
}

bool export(const char *filename)
{
	struct unityfs uf = {0};
	const char *base = basename(filename);
	bool ret = false;
	struct reader *reader;
	struct writer *writer;

	LOG("File %s\n", base);

	reader = reader_create_file_reader(filename);
	if (reader == nullptr)
		return false;

	writer = writer_create_memory_writer();
	if (writer == nullptr) {
		reader_free(reader);
		return false;
	}

	uf.reader = reader;
	uf.writer = writer;

	if (!bundle_read_header_and_blocks(&uf)) {
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	if (bundle_unpack(&uf)) {
		struct serialized *s = serialized_from_memory(writer->data, writer->size);

		if (s) {
			ret = export_data(s);
			bundle_free(&uf);
			serialized_free(s);
		}
	}
	bundle_free(&uf);

	writer_free(writer);
	reader_free(reader);

	return ret;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
