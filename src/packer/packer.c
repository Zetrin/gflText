#define _GNU_SOURCE
#include "packer.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

bool verbose = false;

_Noreturn static void help(void)
{
	printf("Usage: gfl [VERBOSE] MODE FILES...\n"
	       "  -h, --help     Show this message\n"
	       "  -v, --verbose  Show more info\n"
	       "  -l, --list     List content of bundle\n"
	       "  -e, --export   Export supported data\n"
	       "  -i, --import   Replace text or font in bundle\n"
	       "  -d, --decrypt  Decrypt GFL2:Exilium bundle file\n"
	       "  -u, --unpack   Rebuild bundle without compression\n"
	       "  -s, --save     Save serialied data as binary file\n"
	);
	exit(0);
}

int main(int argc, char *argv[])
{
	int next_opt;
	static const char *const short_opts = "hvl:u:d:e:i:s:";
	static const struct option long_opts[] = {
	    {"help",    no_argument,       0, 'h'},
	    {"verbose", no_argument,       0, 'v'},
	    {"list",    required_argument, 0, 'l'},
	    {"export",  required_argument, 0, 'e'},
	    {"import",  required_argument, 0, 'i'},
	    {"decrypt", required_argument, 0, 'd'},
	    {"unpack",  required_argument, 0, 'u'},
	    {"save",    required_argument, 0, 's'},
	    {nullptr,   0,                 0, 0  }
	};

	do {
		next_opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

		switch (next_opt) {
			case 'h':
				help();
				break;
			case 'v':
				verbose = true;
				break;
			case 'l':
				return list(optarg);
			case 'u':
				return unpack(optarg);
			case 'd':
				return decrypt(optarg);
			case 'e':
				return export(optarg);
			case 's':
				return dump(optarg);
			case 'i':
				break;
			case '?':
				break;
			case -1:
				break;
			default:
				return EXIT_FAILURE;
		}

	} while (next_opt != -1);

	return EXIT_SUCCESS;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
