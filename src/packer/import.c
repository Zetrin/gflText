#ifdef IMPORT
static bool is_same_data(void *ldt, size_t lsz, void *rdt, size_t rsz)
{
	uint32_t lcrc = crc32c_value((uint8_t *)ldt, lsz);
	uint32_t rcrc = crc32c_value((uint8_t *)rdt, rsz);

	return lcrc == rcrc;
}

static int32_t read_file_data(const char *path, void **buffer, uint32_t *bsize)
{
	int ret;
	struct stat info;
	int fd;
	off_t file_size;

	ret = stat(path, &info);
	if (ret == -1) {
		if (errno != ENOENT)
			fprintf(stderr, "Could not access file %s: %s\n", path, strerror(errno));
		return -1;
	}

	fd = open(path, O_RDONLY, S_IRWXU);
	if (fd == -1) {
		fprintf(stderr, "Could not open file %s: %s\n", path, strerror(errno));
		return -1;
	}

	file_size = lseek(fd, 0, SEEK_END);
	if (file_size == -1) {
		perror("I/O error");
		close(fd);
		return -1;
	}

	lseek(fd, 0, SEEK_SET);

	if (*buffer == nullptr) {
		*buffer = malloc(file_size);
		if (*buffer == nullptr) {
			perror("Could not allocate memory for buffer");
			return -1;
		}
		*bsize = file_size;
	}

	if ((uint32_t)file_size > *bsize) {
		void *new = realloc(*buffer, file_size);
		if (new == nullptr) {
			fprintf(stderr, "Could not extract %s: %s\n", path, strerror(errno));
			close(fd);
			return -1;
		}

		*buffer = new;
		*bsize = file_size;
	}

	if (read(fd, *buffer, file_size) != file_size) {
		fprintf(stderr, "Failed to read file %s: %s\n", path, strerror(errno));
		close(fd);
		return -1;
	}

	close(fd);

	return file_size;
}

int import_data(serialized *s, bool force)
{
	char path[PATH_MAX] = {0};
	int ret = 0;
	int res = -1;
	void *buffer = nullptr;
	uint32_t buffer_sz = 0;

	if (s->bundle_name == nullptr) {
		fprintf(stderr, "Not valid bundle. Can not import or export data\n");
		goto clean;
	}

	for (uint32_t i = 0; i < s->assets.c; i++) {
		void *new_data = nullptr;
		uint32_t new_size = 0;
		ssize_t file_size = 0;
		struct text *text = nullptr;
		struct font *font = nullptr;
		size_t len = 0;

		len = strlen(s->bundle_name) + 1 + s->assets.v[i].path_length + 1;

		ret = snprintf(path, len, "%s/%s", s->bundle_name, s->assets.v[i].path);
		if (ret < 0)
			continue;

		path[ret] = '\0';

		switch (s->assets.v[i].type->class_id) {
			case CLASSID_TEXTASSET:
				// text = parse_text_asset(s, s->assets.v[i].data, s->assets.v[i].size);

				file_size = read_file_data(path, &buffer, &buffer_sz);
				if (file_size < 0) {
					// free(text);
					continue;
				}

				if (!force && is_same_data(buffer, file_size, text->body, text->body_length)) {
					// free(text);
					continue;
				}

				text->body = buffer;
				text->body_length = file_size;
				new_data = pack_text_asset(s, text, &new_size);

				free(text);

				break;
			case CLASSID_FONT:
				// font = parse_font_asset(s, s->assets.v[i].data, s->assets.v[i].size);

				if (font->data == 0) {
					free(font);
					continue;
				}

				file_size = read_file_data(path, &buffer, &buffer_sz);
				if (file_size < 0) {
					free(font);
					continue;
				}

				if (!force && is_same_data(buffer, file_size, font->data, font->data_length)) {
					free(font);
					continue;
				}

				font->data = buffer;
				font->data_length = file_size;
				new_data = pack_font_asset(s, font, &new_size);

				free(font);

				break;
			default:
				continue;
		}

		printf("\tImport %s\n", path);

		// s->assets.v[i].data = new_data;
		// s->assets.v[i].size = new_size;
	}

	res = 0;

clean:
	free(buffer);
	return res;
}
#endif
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
