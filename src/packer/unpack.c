#define _GNU_SOURCE
#include <limits.h>
#include <stdio.h>
#include <string.h>

#include "assets/assetbundle.h"
#include "common.h"
#include "io/reader.h"
#include "io/writer.h"
#include "serialized.h"
#include "unityfs.h"

bool unpack(const char *filename)
{
	unityfs uf = {0};
	const char *base = basename(filename);
	bool ret = false;
	struct reader *reader;
	struct writer *writer;

	LOG("File %s\n", base);

	reader = reader_create_file_reader(filename);
	if (reader == nullptr)
		return false;

	writer = writer_create_memory_writer();
	if (writer == nullptr) {
		reader_free(reader);
		return false;
	}

	uf.reader = reader;
	uf.writer = writer;

	if (!bundle_read_header_and_blocks(&uf)) {
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	if (!bundle_unpack(&uf)) {
		bundle_free(&uf);
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	struct serialized *s = serialized_from_memory(uf.writer->data, uf.writer->size);
	if (s == nullptr) {
		bundle_free(&uf);
		reader_free(reader);
		writer_free(writer);
		return false;
	}

	const char *bundle_name = s->catalog->bundle_name;
	char path[PATH_MAX];

	snprintf(path, PATH_MAX, "%s.ab", bundle_name);

	struct writer *out = writer_create_file_writer(path);

	writer_set_big_endian(out);
	writer_put_bytes(out, "UnityFS\0", 8);
	writer_put_32(out, uf.version);
	writer_put_string(out, uf.u_version);
	writer_align(out, 2);
	writer_put_string(out, uf.u_revision);
	writer_align(out, 2);
	// const uint64_t size_offset = out->offset;
	writer_put_32(out, 0);

	reader_free(s->reader);
	serialized_free(s);

	bundle_free(&uf);

	writer_free(writer);
	reader_free(reader);

	return ret;
}
/* vim: set tabstop=4 shiftwidth=4 noexpandtab smartindent ff=unix: */
